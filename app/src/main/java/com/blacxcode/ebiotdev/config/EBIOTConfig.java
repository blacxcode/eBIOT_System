package com.blacxcode.ebiotdev.config;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class EBIOTConfig {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 0; //100
    public static final int NOTIFICATION_ID_BIG_IMAGE = 1; //101

    public static final String SHARED_PREF = "prefs.config.ebiotdev";

    public static final String DB_EBIOT = "ebiotdb";
    public static final String DB_EBIOTCLIENT = "ebiotclient";
    public static final String DB_EBIOTDATA = "ebiotdata";
}
