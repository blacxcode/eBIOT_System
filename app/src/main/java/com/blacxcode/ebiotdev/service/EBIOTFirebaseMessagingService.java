package com.blacxcode.ebiotdev.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.blacxcode.ebiotdev.activity.EBIOTMainActivity;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by blacXcode on 5/7/2017.
 */

public class EBIOTFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = EBIOTFirebaseMessagingService.class.getSimpleName();
    private EBIOTNotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.public Context context;
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleNotification(json.getString("title"), json.getString("message"));
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String title, String message) {
        SharedPreferences sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);

        if (!EBIOTNotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent intent = new Intent(EBIOTConfig.PUSH_NOTIFICATION);
            intent.putExtra("title", title);
            intent.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

            if (sharedPreferences.getString("EBIOTCheckSound", "ON").equals("ON")) {
                // play notification sound
                notificationUtils = new EBIOTNotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }
        } else {
            // If the app is in background, firebase itself handles the notification
            Log.e(TAG, "isBackground: ");
            Intent intent = new Intent(getApplicationContext(), EBIOTMainActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("message", message);

            // check for image attachment
            showNotificationBG(getApplicationContext(), title, message, intent);
        }
    }

    private void handleDataMessage(JSONObject json) {
        SharedPreferences sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("isbackground");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!EBIOTNotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(EBIOTConfig.PUSH_NOTIFICATION);
                pushNotification.putExtra("title", title);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                notificationUtils = new EBIOTNotificationUtils(getApplicationContext());
                if (sharedPreferences.getString("EBIOTCheckSound", "ON").equals("ON")) {
                    notificationUtils.playNotificationSound();
                }
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), EBIOTMainActivity.class);
                resultIntent.putExtra("title", title);
                resultIntent.putExtra("message", message);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationBG(Context context, String title, String message, Intent intent) {
        notificationUtils = new EBIOTNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        notificationUtils.showNotificationBG(title, message, intent);
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        SharedPreferences sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);

        notificationUtils = new EBIOTNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);

        if (sharedPreferences.getString("EBIOTCheckSound", "ON").equals("ON")) {
            // play notification sound
            notificationUtils.playNotificationSound();
        }
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new EBIOTNotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}

