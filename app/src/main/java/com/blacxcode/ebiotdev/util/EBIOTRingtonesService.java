package com.blacxcode.ebiotdev.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import com.blacxcode.ebiotdev.config.EBIOTConfig;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by blacXcode on 5/17/2017.
 */

public class EBIOTRingtonesService {
    public Context context;
    public static MediaPlayer mediaPlayer;

    public EBIOTRingtonesService(Context context) {
        this.context = context;
    }

    public void playRingtones(Uri alarmUri) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        Float getLeftVol = sharedPreferences.getFloat("EBIOTLeftVol", 0.0f);
        Float getRightVol = sharedPreferences.getFloat("EBIOTRightVol", 0.0f);

        stopRingtones();

        mediaPlayer = MediaPlayer.create(context, alarmUri);

        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setLooping(false);
            mediaPlayer.setVolume(getLeftVol, getRightVol);
            mediaPlayer.start();
        }
    }
    public void resumeRingtones(Uri alarmUri) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        Float getLeftVol = sharedPreferences.getFloat("EBIOTLeftVol", 0.0f);
        Float getRightVol = sharedPreferences.getFloat("EBIOTRightVol", 0.0f);
        int length = sharedPreferences.getInt("EBIOTMediaPlayerLength", 0);

        mediaPlayer = MediaPlayer.create(context, alarmUri);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(false);
        mediaPlayer.setVolume(getLeftVol, getRightVol);
        mediaPlayer.seekTo(length);
        mediaPlayer.start();
    }

    public void pauseRingtones(Uri alarmUri) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);

        mediaPlayer = MediaPlayer.create(context, alarmUri);
        mediaPlayer.pause();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("EBIOTMediaPlayerLength", mediaPlayer.getCurrentPosition());
        edit.apply();

    }

    public void stopRingtones() {
        if(mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }
    }

    public Boolean isRingtones() {
        return mediaPlayer.isPlaying();
    }
}