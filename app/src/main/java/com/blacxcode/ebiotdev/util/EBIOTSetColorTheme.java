package com.blacxcode.ebiotdev.util;

import com.blacxcode.ebiotdev.R;

public class EBIOTSetColorTheme {

    private static final String TAG_PINK = "EBIOTColorPink";
    private static final String TAG_PURPLE = "EBIOTColorPurple";
    private static final String TAG_DEEPPURPLE = "EBIOTColorDeepPurple";
    private static final String TAG_BLUE = "EBIOTColorBlue";
    private static final String TAG_LIGHTBLUE = "EBIOTColorLightBlue";
    private static final String TAG_CYAN = "EBIOTColorCyan";
    private static final String TAG_LIGHTGREEN = "EBIOTColorLightGreen";
    private static final String TAG_LIME = "EBIOTColorLime";
    private static final String TAG_GRAY = "EBIOTColorGrey";
    private static final String TAG_BLUEGRAY = "EBIOTColorBlueGrey";
    private static final String TAG_BROWN = "EBIOTColorBrown";
    private static final String TAG_TEAL = "EBIOTColorTeal";
    private static final String TAG_INDIGO = "EBIOTColorIndigo";
    private static final String TAG_RED = "EBIOTColorRed";

    public static int getColorPrimary(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPrimaryPink;

            case TAG_PURPLE :
                return R.color.colorPrimaryPurple;

            case TAG_DEEPPURPLE :
                return R.color.colorPrimaryDeepPurple;

            case TAG_BLUE :
                return R.color.colorPrimaryBlue;

            case TAG_LIGHTBLUE :
                return R.color.colorPrimaryLightBlue;

            case TAG_CYAN :
                return R.color.colorPrimaryCyan;

            case TAG_LIGHTGREEN :
                return R.color.colorPrimaryLightGreen;

            case TAG_LIME :
                return R.color.colorPrimaryLime;

            case TAG_GRAY :
                return R.color.colorPrimaryGrey;

            case TAG_BLUEGRAY :
                return R.color.colorPrimaryBlueGrey;

            case TAG_BROWN :
                return R.color.colorPrimaryBrown;

            case TAG_TEAL :
                return R.color.colorPrimaryTeal;

            case TAG_INDIGO :
                return R.color.colorPrimaryIndigo;

            case TAG_RED :
                return R.color.colorPrimaryRed;

            default:
                return R.color.colorPrimaryLightBlue;
        }
    }

    public static int getColorPrimaryDark(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPrimaryDarkPink;

            case TAG_PURPLE :
                return R.color.colorPrimaryDarkPurple;

            case TAG_DEEPPURPLE :
                return R.color.colorPrimaryDarkDeepPurple;

            case TAG_BLUE :
                return R.color.colorPrimaryDarkBlue;

            case TAG_LIGHTBLUE :
                return R.color.colorPrimaryDarkLightBlue;

            case TAG_CYAN :
                return R.color.colorPrimaryDarkCyan;

            case TAG_LIGHTGREEN :
                return R.color.colorPrimaryDarkLightGreen;

            case TAG_LIME :
                return R.color.colorPrimaryDarkLime;

            case TAG_GRAY :
                return R.color.colorPrimaryDarkGrey;

            case TAG_BLUEGRAY :
                return R.color.colorPrimaryDarkBlueGrey;

            case TAG_BROWN :
                return R.color.colorPrimaryDarkBrown;

            case TAG_TEAL :
                return R.color.colorPrimaryDarkTeal;

            case TAG_INDIGO :
                return R.color.colorPrimaryDarkIndigo;

            case TAG_RED :
                return R.color.colorPrimaryDarkRed;

            default:
                return R.color.colorPrimaryDarkPurple;
        }
    }

    public static int getColorAccent(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorAccentPink;

            case TAG_PURPLE :
                return R.color.colorAccentPurple;

            case TAG_DEEPPURPLE :
                return R.color.colorAccentDeepPurple;

            case TAG_BLUE :
                return R.color.colorAccentBlue;

            case TAG_LIGHTBLUE :
                return R.color.colorAccentLightBlue;

            case TAG_CYAN :
                return R.color.colorAccentCyan;

            case TAG_LIGHTGREEN :
                return R.color.colorAccentLightGreen;

            case TAG_LIME :
                return R.color.colorAccentLime;

            case TAG_GRAY :
                return R.color.colorAccentGrey;

            case TAG_BLUEGRAY :
                return R.color.colorAccentBlueGrey;

            case TAG_BROWN :
                return R.color.colorAccentBrown;

            case TAG_TEAL :
                return R.color.colorAccentTeal;

            case TAG_INDIGO :
                return R.color.colorAccentIndigo;

            case TAG_RED :
                return R.color.colorAccentRed;

            default:
                return R.color.colorAccentPurple;
        }
    }

    public static int getColor100(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink100;

            case TAG_PURPLE :
                return R.color.colorPurple100;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple100;

            case TAG_BLUE :
                return R.color.colorBlue100;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue100;

            case TAG_CYAN :
                return R.color.colorCyan100;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen100;

            case TAG_LIME :
                return R.color.colorLime100;

            case TAG_GRAY :
                return R.color.colorGrey100;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey100;

            case TAG_BROWN :
                return R.color.colorBrown100;

            case TAG_TEAL :
                return R.color.colorTeal100;

            case TAG_INDIGO :
                return R.color.colorIndigo100;

            case TAG_RED :
                return R.color.colorRed100;

            default:
                return R.color.colorPurple100;
        }
    }

    public static int getColor200(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink200;

            case TAG_PURPLE :
                return R.color.colorPurple200;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple200;

            case TAG_BLUE :
                return R.color.colorBlue200;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue200;

            case TAG_CYAN :
                return R.color.colorCyan200;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen200;

            case TAG_LIME :
                return R.color.colorLime200;

            case TAG_GRAY :
                return R.color.colorGrey200;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey200;

            case TAG_BROWN :
                return R.color.colorBrown200;

            case TAG_TEAL :
                return R.color.colorTeal200;

            case TAG_INDIGO :
                return R.color.colorIndigo200;

            case TAG_RED :
                return R.color.colorRed200;

            default:
                return R.color.colorPurple200;
        }
    }

    public static int getColor300(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink300;

            case TAG_PURPLE :
                return R.color.colorPurple300;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple300;

            case TAG_BLUE :
                return R.color.colorBlue300;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue300;

            case TAG_CYAN :
                return R.color.colorCyan300;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen300;

            case TAG_LIME :
                return R.color.colorLime300;

            case TAG_GRAY :
                return R.color.colorGrey300;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey300;

            case TAG_BROWN :
                return R.color.colorBrown300;

            case TAG_TEAL :
                return R.color.colorTeal300;

            case TAG_INDIGO :
                return R.color.colorIndigo300;

            case TAG_RED :
                return R.color.colorRed300;

            default:
                return R.color.colorPurple300;
        }
    }

    public static int getColor400(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink400;

            case TAG_PURPLE :
                return R.color.colorPurple400;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple400;

            case TAG_BLUE :
                return R.color.colorBlue400;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue400;

            case TAG_CYAN :
                return R.color.colorCyan400;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen400;

            case TAG_LIME :
                return R.color.colorLime400;

            case TAG_GRAY :
                return R.color.colorGrey400;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey400;

            case TAG_BROWN :
                return R.color.colorBrown400;

            case TAG_TEAL :
                return R.color.colorTeal400;

            case TAG_INDIGO :
                return R.color.colorIndigo400;

            case TAG_RED :
                return R.color.colorRed400;

            default:
                return R.color.colorPurple400;
        }
    }

    public static int getColor500(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink500;

            case TAG_PURPLE :
                return R.color.colorPurple500;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple500;

            case TAG_BLUE :
                return R.color.colorBlue500;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue500;

            case TAG_CYAN :
                return R.color.colorCyan500;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen500;

            case TAG_LIME :
                return R.color.colorLime500;

            case TAG_GRAY :
                return R.color.colorGrey500;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey500;

            case TAG_BROWN :
                return R.color.colorBrown500;

            case TAG_TEAL :
                return R.color.colorTeal500;

            case TAG_INDIGO :
                return R.color.colorIndigo500;

            case TAG_RED :
                return R.color.colorRed500;

            default:
                return R.color.colorPurple500;
        }
    }

    public static int getColor600(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink600;

            case TAG_PURPLE :
                return R.color.colorPurple600;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple600;

            case TAG_BLUE :
                return R.color.colorBlue600;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue600;

            case TAG_CYAN :
                return R.color.colorCyan600;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen600;

            case TAG_LIME :
                return R.color.colorLime600;

            case TAG_GRAY :
                return R.color.colorGrey600;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey600;

            case TAG_BROWN :
                return R.color.colorBrown600;

            case TAG_TEAL :
                return R.color.colorTeal600;

            case TAG_INDIGO :
                return R.color.colorIndigo600;

            case TAG_RED :
                return R.color.colorRed600;

            default:
                return R.color.colorPurple600;
        }
    }

    public static int getColor700(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink700;

            case TAG_PURPLE :
                return R.color.colorPurple700;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple700;

            case TAG_BLUE :
                return R.color.colorBlue700;

            case TAG_CYAN :
                return R.color.colorCyan700;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue700;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen700;

            case TAG_LIME :
                return R.color.colorLime700;

            case TAG_GRAY :
                return R.color.colorGrey700;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey700;

            case TAG_BROWN :
                return R.color.colorBrown700;

            case TAG_TEAL :
                return R.color.colorTeal700;

            case TAG_INDIGO :
                return R.color.colorIndigo700;

            case TAG_RED :
                return R.color.colorRed700;

            default:
                return R.color.colorPurple700;
        }
    }

    public static int getColor800(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink800;

            case TAG_PURPLE :
                return R.color.colorPurple800;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple800;

            case TAG_BLUE :
                return R.color.colorBlue800;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue800;

            case TAG_CYAN :
                return R.color.colorCyan800;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen800;

            case TAG_LIME :
                return R.color.colorLime800;

            case TAG_GRAY :
                return R.color.colorGrey800;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey800;

            case TAG_BROWN :
                return R.color.colorBrown800;

            case TAG_TEAL :
                return R.color.colorTeal800;

            case TAG_INDIGO :
                return R.color.colorIndigo800;

            case TAG_RED :
                return R.color.colorRed800;

            default:
                return R.color.colorPurple800;
        }
    }

    public static int getColor900(String getColor) {
        switch (getColor) {
            case TAG_PINK :
                return R.color.colorPink900;

            case TAG_PURPLE :
                return R.color.colorPurple900;

            case TAG_DEEPPURPLE :
                return R.color.colorDeepPurple900;

            case TAG_BLUE :
                return R.color.colorBlue900;

            case TAG_LIGHTBLUE :
                return R.color.colorLightBlue900;

            case TAG_CYAN :
                return R.color.colorCyan900;

            case TAG_LIGHTGREEN :
                return R.color.colorLightGreen900;

            case TAG_LIME :
                return R.color.colorLime900;

            case TAG_GRAY :
                return R.color.colorGrey900;

            case TAG_BLUEGRAY :
                return R.color.colorBlueGrey900;

            case TAG_BROWN :
                return R.color.colorBrown900;

            case TAG_TEAL :
                return R.color.colorTeal900;

            case TAG_INDIGO :
                return R.color.colorIndigo900;

            case TAG_RED :
                return R.color.colorRed900;

            default:
                return R.color.colorPurple900;
        }
    }
}
