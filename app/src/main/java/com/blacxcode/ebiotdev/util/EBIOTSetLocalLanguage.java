package com.blacxcode.ebiotdev.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import java.util.Locale;

public class EBIOTSetLocalLanguage {

    private static final int TAG_IN = 0;
    private static final int TAG_EN = 1;
    private static final int TAG_JA = 2;

    public static String getLocale(int getLocale) {
        switch (getLocale) {
            case TAG_IN:
                return "id";

            case TAG_EN :
                return "en";

            case TAG_JA :
                return "ja";

            default:
                return "en";
        }
    }

    /*change language at Run-time*/
    //use method like that:
    //setLocale("en");
    public static void setLocale(String lang, Context context) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            configuration.setLocale(locale);
            context.createConfigurationContext(configuration);
        } else {
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configuration.locale = locale;
            resources.updateConfiguration(configuration, displayMetrics);
        }
    }

//    @SuppressWarnings("deprecation")
//    public Locale getSystemLocaleLegacy(Configuration config){
//        return config.locale;
//    }
//
//    @TargetApi(Build.VERSION_CODES.N)
//    public Locale getSystemLocale(Configuration config){
//        return config.getLocales().get(0);
//    }
//
//    @SuppressWarnings("deprecation")
//    private static  void setSystemLocaleLegacy(Configuration config, Locale locale){
//        config.locale = locale;
//    }
//
//    @TargetApi(Build.VERSION_CODES.N)
//    private static void setSystemLocale(Configuration config, Locale locale){
//        config.setLocale(locale);
//    }
//
//    public static void setLocale(String lang, Context mContext){
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            setSystemLocale(config, locale);
//        }else{
//            setSystemLocaleLegacy(config, locale);
//        }
//        mContext.getApplicationContext().getResources().updateConfiguration(config,
//                mContext.getResources().getDisplayMetrics());
//    }
}
