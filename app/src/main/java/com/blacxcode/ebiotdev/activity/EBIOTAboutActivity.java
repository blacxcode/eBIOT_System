package com.blacxcode.ebiotdev.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.firebase.messaging.FirebaseMessaging;

public class EBIOTAboutActivity extends AppCompatActivity {
    protected BroadcastReceiver mRegistrationBroadcastReceiver;
    protected SharedPreferences sharedPreferences;
    protected ViewPager viewPager;
    protected LinearLayout dotsLayout;
    protected MyViewPagerAdapter myViewPagerAdapter;
    protected TextView[] dots;
    protected TextView whatsnew_header, whatsnew_content, about_app_name, about_app_version, about_content, acknowledgements_content;
    protected CardView whatsnew_cardview, about_cardview, acknowledgements_cardview;
    protected ImageView logo;

    protected static final int SLIDER_WHATSNEW = 0;
    protected static final int SLIDER_ABOUT = 1;
    protected static final int SLIDER_LICENSE = 2;

    private String[] activityTitles;
    private String getThemes = "EBIOTColorPink";
    private int[] layouts;
    private int slideItemIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        activityTitles = getResources().getStringArray(R.array.slide_item_activity_titles);

        setUI();
        setToolbarTitle();

        layouts = new int[] {
                R.layout.slider_whatsnew,
                R.layout.slider_about,
                R.layout.slider_license
        };

        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(EBIOTConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(EBIOTConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(EBIOTConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");

                    Intent notify = new Intent (EBIOTAboutActivity.this, EBIOTNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    startActivity(notify);
                }
            }
        };
    }


    void setUI () {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null)));
        }
    }

    private void setToolbarTitle() {
        //getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(activityTitles[slideItemIndex]);
        }
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int colorsActive;
        int colorsInactive;

        colorsActive = ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null);
        colorsInactive = ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                dots[i].setText(Html.fromHtml("&#8226;", Html.FROM_HTML_MODE_LEGACY));
            } else {
                dots[i].setText(Html.fromHtml("&#8226;"));
            }
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            slideItemIndex = position;
            setToolbarTitle();
            loadContent (position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            setToolbarTitle();
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            setToolbarTitle();
        }
    };

    @SuppressLint("Deprecation")
    private void loadContent (int position) {
        whatsnew_header = (TextView) findViewById(R.id.whatsnew_header);
        whatsnew_content = (TextView) findViewById(R.id.whatsnew_content);
        about_app_name = (TextView) findViewById(R.id.about_app_name);
        about_app_version = (TextView) findViewById(R.id.about_app_version);
        about_content = (TextView) findViewById(R.id.about_content);
        acknowledgements_content = (TextView) findViewById(R.id.license_content);
        whatsnew_cardview = (CardView) findViewById(R.id.whatsnew_cardview);
        about_cardview = (CardView) findViewById(R.id.about_cardview);
        acknowledgements_cardview = (CardView) findViewById(R.id.license_cardview);
        logo = (ImageView) findViewById(R.id.logo);

        switch(position) {
            case SLIDER_WHATSNEW:
                String whatsnewheader = getResources().getString(R.string.slide_whatsnew) + getResources().getString(R.string.versionName);

                whatsnew_cardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));

                whatsnew_header.setText(whatsnewheader);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    whatsnew_content.setText(Html.fromHtml(getResources().getString(R.string.whats_new),Html.FROM_HTML_MODE_LEGACY));
                    whatsnew_content.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    whatsnew_content.setText(Html.fromHtml(getResources().getString(R.string.whats_new)));
                    whatsnew_content.setMovementMethod(LinkMovementMethod.getInstance());
                }
                break;

            case SLIDER_ABOUT:
                String AppVersion = getResources().getString(R.string.app_version) + getResources().getString(R.string.versionName);

                about_cardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));

                about_app_name.setText(getResources().getString(R.string.app_name));
                about_app_version.setText(AppVersion);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    about_content.setText(Html.fromHtml(getResources().getString(R.string.about),Html.FROM_HTML_MODE_LEGACY));
                    about_content.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    about_content.setText(Html.fromHtml(getResources().getString(R.string.about)));
                    about_content.setMovementMethod(LinkMovementMethod.getInstance());
                }

                logo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AnimationSet animationSet = new AnimationSet(false);
                        animationSet.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sequential_about_show_up));
                        animationSet.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sequential_about_click));
                        logo.startAnimation(animationSet);
                    }
                });
                break;

            case SLIDER_LICENSE:
                acknowledgements_cardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    acknowledgements_content.setText(Html.fromHtml(getResources().getString(R.string.license),Html.FROM_HTML_MODE_LEGACY));
                    acknowledgements_content.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    acknowledgements_content.setText(Html.fromHtml(getResources().getString(R.string.license)));
                    acknowledgements_content.setMovementMethod(LinkMovementMethod.getInstance());
                }
                break;
        }
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        MyViewPagerAdapter() {
            // Required empty public constructor
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);

            container.addView(view);
            loadContent (position);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart () {
        super.onStart();
        setUI();
    }

    @Override
    public void onResume () {
        super.onResume();
        setUI();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        EBIOTNotificationUtils.clearNotifications(this);
    }

    @Override
    public void onPause () {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
