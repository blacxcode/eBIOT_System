package com.blacxcode.ebiotdev.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.service.EBIOTFirebaseInstanceIDService;
import com.blacxcode.ebiotdev.sync.EBIOTDBParseData;
import com.blacxcode.ebiotdev.sync.EBIOTHTTPHandler;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import static com.blacxcode.ebiotdev.sync.EBIOTJSONParseData.readFromExternalFile;
import static com.blacxcode.ebiotdev.sync.EBIOTJSONParseData.readFromInternalCache;
import static com.blacxcode.ebiotdev.sync.EBIOTJSONParseData.writeToExternalFile;
import static com.blacxcode.ebiotdev.sync.EBIOTJSONParseData.writeToInternalCache;


/**
 * Created by blacXcode on 5/7/2017.
 */

public class EBIOTSplashActivity extends AppCompatActivity {
    protected static final String TAG = EBIOTFirebaseInstanceIDService.class.getSimpleName();
    protected LinearLayout linearLayout;
    protected ImageView splash;
    protected TextView AppName, AppVersion;
    protected Thread splashTread;
    protected SharedPreferences sharedPreferences;
    protected DatabaseReference mFirebaseDBClient;
    protected FirebaseDatabase mFirebaseInstance;

    private static final int REQUEST_ID_READ_PERMISSION = 100;
    private static final int REQUEST_ID_WRITE_PERMISSION = 200;

    private String dirName = "/ebiotdev";
    private String fileName = "deviceid.dat";
    private String deviceid = "";

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSLUCENT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        AppName = findViewById(R.id.AppName);
        AppVersion = findViewById(R.id.AppVersion);
        linearLayout = findViewById(R.id.splash_layout);
        splash = findViewById(R.id.splash);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDBClient = mFirebaseInstance.getReference(EBIOTConfig.DB_EBIOT);

        deviceid = checkDeviceID();
        Log.e(TAG, "checkDeviceID infoid : " + deviceid);

        setUI();

        if (sharedPreferences.getBoolean("EBIOTAccess", false)) {
            runAnimated();
        }
    }

    private void StartAnimations() {
        linearLayout.clearAnimation();
        linearLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_alpha));

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sequential_splash_show_up));
        animationSet.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sequential_splash_click));
        splash.startAnimation(animationSet);

        AppName.clearAnimation();
        AppName.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_appname));

        AppVersion.clearAnimation();
        AppVersion.startAnimation(AnimationUtils.loadAnimation(this, R.anim.sequential_splash_appversion));

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;

                    while (waited < 6000) {
                        sleep(100);
                        waited += 100;
                    }

                    Intent intent = new Intent(EBIOTSplashActivity.this, EBIOTMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    EBIOTSplashActivity.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    EBIOTSplashActivity.this.finish();
                }
            }
        };
        splashTread.start();
    }

    private class getData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linearLayout.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            EBIOTHTTPHandler handler = new EBIOTHTTPHandler();
            String database_url = sharedPreferences.getString("EBIOTHostname", getResources().getString(R.string.database_url));

            if (!database_url.equals("")) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("EBIOTHostname", database_url);
                edit.apply();
            }

            String JSONData = handler.makeServiceCall(database_url + "ebiotdb.json", null);

            if (JSONData == null) {
                JSONData = readFromInternalCache("ebiotdb.json");
            } else {
                writeToInternalCache("ebiotdb.json", JSONData);
            }

            parseDataIoTClient(JSONData);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            linearLayout.setVisibility(View.VISIBLE);
            StartAnimations();
        }
    }

    private void parseDataIoTClient(String JSONData) {
        EBIOTDBParseData dbParseData = new EBIOTDBParseData();
        deviceid = getDeviceID();
        String devicekey = getDeviceKey();

        int id_add = 0;
        Boolean newuser = false;

        try {
            JSONObject jdbObj = new JSONObject(JSONData);
            JSONArray dataArray = jdbObj.getJSONArray("ebiotclient");

            for (int i = 0; i < dataArray.length(); i++) {
                Log.e(TAG, "Client ID 0 : " + String.valueOf(i) + ", total : " + dataArray.length());
                String pDeviceId = dataArray.getJSONObject(i).getString("deviceid");
                String pDeviceKey = dataArray.getJSONObject(i).getString("devicekey");
                String pRemoteAllow = dataArray.getJSONObject(i).getString("remoteallow");
                String pUserName = dataArray.getJSONObject(i).getString("username");

                if (pDeviceId.equals(deviceid)) {
                    if(pDeviceKey.equals(devicekey)) {
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("clientid", String.valueOf(i));
                        edit.putString("deviceid", pDeviceId);
                        edit.putString("devicekey", pDeviceKey);
                        edit.putString("remoteallow", pRemoteAllow);
                        edit.putString("username", pUserName);
                        edit.apply();

                        Log.e(TAG, "Client ID 1 : " + String.valueOf(i));
                    } else {
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("clientid", String.valueOf(i));
                        edit.putString("deviceid", pDeviceId);
                        edit.putString("devicekey", devicekey);
                        edit.putString("remoteallow", pRemoteAllow);
                        edit.putString("username", pUserName);
                        edit.apply();

                        dbParseData.updateClient(mFirebaseDBClient, String.valueOf(i), pDeviceId, devicekey, pRemoteAllow, pUserName);

                        Log.e(TAG, "Client ID 2 : " + String.valueOf(i));
                    }

                    newuser = false;
                    break;
                } else if (!pDeviceId.equals("") && !pDeviceId.equals(deviceid)) {
                    id_add = i + 1;

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("clientid", String.valueOf(id_add));
                    edit.apply();

                    newuser = true;

                    Log.e(TAG, "Client ID 3 : " + String.valueOf(id_add));
                } else if (pDeviceId.equals("")) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("clientid", String.valueOf(i));
                    edit.apply();

                    dbParseData.updateClient(mFirebaseDBClient, String.valueOf(i), deviceid, devicekey, "false", "Guest " + i);

                    Log.e(TAG, "Client ID 4 : " + String.valueOf(i));
                    break;
                }
            }

            if (newuser) {
                dbParseData.updateClient(mFirebaseDBClient, String.valueOf(id_add), deviceid, devicekey, "false", "Guest " + id_add);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //dbParseData.updateClient(mFirebaseDBClient, "0", deviceid, devicekey, "false", "");
            //dbParseData.updateClient(mFirebaseDBClient, String.valueOf(id_add), deviceid, devicekey, "false", "Guest " + id_add);

            Log.e(TAG, "Client Error ...");
        }

        try {
            JSONObject jdbObj = new JSONObject(JSONData);
            JSONObject dataObj = jdbObj.getJSONObject("ebiotdata");

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt("iotdacs", dataObj.getInt("iotdacs"));
            edit.putString("iotsnd", dataObj.getString("iotsnd"));
            edit.putString("iothacs", dataObj.getString("iothacs"));
            edit.putString("iotmsg", dataObj.getString("iotmsg"));
            edit.apply();

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Data Error ...");
        }
    }

    public String getDeviceID() {
        deviceid = askPermissionAndReadFile();
        //askPermissionAndWriteFile();

        Log.e(TAG, "getDeviceID (askPermissionAndReadFile) infoid : " + deviceid);

        if (deviceid.equals("")) {
            deviceid = UUID.randomUUID().toString();
            askPermissionAndWriteFile();

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("deviceid", deviceid);
            edit.apply();
            Log.e(TAG, "getDeviceID (askPermissionAndWriteFile) infoid : " + deviceid);
        }

        return deviceid;
    }

    public String checkDeviceID() {
        deviceid = askPermissionAndReadFile();
        askPermissionAndWriteFile();

        Log.e(TAG, "checkDeviceID (askPermissionAndReadFile) infoid : " + deviceid);
        return deviceid;
    }

    public String getDeviceKey() {
        String devicekey = sharedPreferences.getString("devicekey", "");
        Log.e(TAG, "getDeviceKey infoid : " + devicekey);

        return devicekey;
    }

    void setUI() {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        linearLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
    }

    void runAnimated() {
        String AppNameText = getResources().getString(R.string.app_name);
        String AppVersionText = getResources().getString(R.string.app_version) + getResources().getString(R.string.versionName);

        AppName.setText(AppNameText);
        AppVersion.setText(AppVersionText);

        new getData().execute();
    }


    // When you have the request results
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        // Note: If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            switch (requestCode) {
                case REQUEST_ID_READ_PERMISSION: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        readFromExternalFile(dirName, fileName);
                        deviceid = askPermissionAndReadFile();

                        runAnimated();

                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putBoolean("EBIOTAccess", true);
                        edit.apply();
                    } else {
                        finishAffinity();
                    }
                }
                case REQUEST_ID_WRITE_PERMISSION: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        writeToExternalFile(dirName, fileName, deviceid);
                    } else {
                        finishAffinity();
                    }
                }
            }
        } else {
            Log.e(TAG, "Permission Cancelled!");
        }
    }

    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {

            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(this, permissionName);


            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }

    private String askPermissionAndReadFile() {
        boolean canRead = this.askPermission(REQUEST_ID_READ_PERMISSION, Manifest.permission.READ_EXTERNAL_STORAGE);
        String content = "";

        if (canRead) {
            content = readFromExternalFile(dirName, fileName);
        }

        return content;
    }

    private void askPermissionAndWriteFile() {
        int REQUEST_ID_WRITE_PERMISSION = 200;
        boolean canWrite = askPermission(REQUEST_ID_WRITE_PERMISSION, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (canWrite) {
            writeToExternalFile(dirName, fileName, deviceid);
        }
    }

    @Override
    public void onStart () {
        super.onStart();
        setUI();
    }

    @Override
    public void onResume () {
        super.onResume();
        setUI();
    }

    @Override
    public void onPause () {
        super.onPause();

    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
