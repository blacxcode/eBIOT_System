package com.blacxcode.ebiotdev.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.fragment.EBIOTACSFragment;
import com.blacxcode.ebiotdev.fragment.EBIOTCoreListenerFragment;
import com.blacxcode.ebiotdev.fragment.EBIOTHomeFragment;
import com.blacxcode.ebiotdev.fragment.EBIOTSNSFragment;
import com.blacxcode.ebiotdev.util.EBIOTDefaultAnimationHandler;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.oguzdev.circularfloatingactionmenu.library.animation.DefaultAnimationHandler;

import java.util.ArrayList;
import java.util.List;

import static com.blacxcode.ebiotdev.R.id.corecontainer;

public class EBIOTMainActivity extends AppCompatActivity {
    protected BroadcastReceiver mRegistrationBroadcastReceiver;
    protected SharedPreferences sharedPreferences;
    protected InterstitialAd mInterstitialAd;
    protected FloatingActionButton fab;
    protected FloatingActionMenu rightLowerMenu;
    protected Toolbar toolbar;
    protected TabLayout tabLayout;
    protected ViewPager viewPager;
    protected FrameLayout frameLayout;
    protected ImageView imgThemes, imgSettings, imgAbout, imgQuit;
    protected SubActionButton btnThemes, btnSettings, btnAbout, btnQuit;
    protected SubActionButton.Builder itemBuilder;
    protected TextView tabHome, tabACS, tabSNS;
    //protected AppBarLayout appBarLayout;
    //protected Translate translate;

    private String getThemes = "EBIOTColorPink";
    private int getTabPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());
        //Toast.makeText(getApplicationContext(), EBIOTSetLocalLanguage.getLocale(getLang), Toast.LENGTH_LONG).show();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        fab = findViewById(R.id.fab_main);
        SetFloatingActionButton();

        viewPager = findViewById(R.id.viewpager_main_act);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        tabLayout = findViewById(R.id.tabs_main_act);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        mInterstitialAd = newInterstitialAd();
        loadInterstitial();

        frameLayout = findViewById(corecontainer);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction trans = fragmentManager.beginTransaction();
        trans.replace(corecontainer, new EBIOTCoreListenerFragment());
        trans.commit();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(EBIOTConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(EBIOTConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(EBIOTConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");

                    Intent notify = new Intent (EBIOTMainActivity.this, EBIOTNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    startActivity(notify);
                }
            }
        };
    }

    private void loadInterstitial() {
        final AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("15C0119EA7504242")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.i("showInterstitial", "Ad did not load");
            //Toast.makeText(getApplicationContext(), "Ad did not load", Toast.LENGTH_SHORT).show();
        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
                Log.i("onAdLoaded", "Ad loaded.");
                //Toast.makeText(getApplicationContext(), "Ad loaded.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.i("onAdFailedToLoad", "Ad failed to load with error code : " + errorCode);

                if (getTabPos == 0) {
                    showFloatingActionButton();
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(getApplicationContext(), "Ad failed to load with error code : " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.i("onAdOpened", "Ad opened.");

                //Toast.makeText(getApplicationContext(), "Ad opened.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                showInterstitial();
                Log.i("onAdClosed", "Ad closed.");

                if (getTabPos == 0) {
                    showFloatingActionButton();
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(getApplicationContext(), "Ad closed.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.i("onAdLeftApplication", "Ad left application.");

                if (getTabPos == 0) {
                    showFloatingActionButton();
                } else {
                    hideFloatingActionButton();
                }
                //Toast.makeText(getApplicationContext(), "Ad left application.", Toast.LENGTH_SHORT).show();
            }
        });
        return interstitialAd;
    }

    private void setupTabIcons() {
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));

        tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setTag(getResources().getString(R.string.fragment_name_home));
        tabHome.setText(getResources().getString(R.string.fragment_name_home));
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_add_alert_white_48dp, 0, 0);
        tabHome.setAnimation(s);
        tabLayout.getTabAt(0).setCustomView(tabHome);

        tabACS = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabACS.setTag(getResources().getString(R.string.fragment_name_acs));
        tabACS.setText(getResources().getString(R.string.fragment_name_acs));
        tabACS.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_settings_remote_white_48dp, 0, 0);
        tabACS.setAnimation(s);
        tabLayout.getTabAt(1).setCustomView(tabACS);

        tabSNS = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabSNS.setTag(getResources().getString(R.string.fragment_name_sns));
        tabSNS.setText(getResources().getString(R.string.fragment_name_sns));
        tabSNS.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_supervisor_account_white_48dp, 0, 0);
        tabSNS.setAnimation(s);
        tabLayout.getTabAt(2).setCustomView(tabSNS);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new EBIOTHomeFragment(), getString(R.string.fragment_name_home));
        adapter.addFrag(new EBIOTACSFragment(), getString(R.string.fragment_name_acs));
        adapter.addFrag(new EBIOTSNSFragment(), getString(R.string.fragment_name_sns));
        viewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {


        @Override
        public void onPageSelected(int position) {
            AnimationSet s = new AnimationSet(false);

            if (position == 0) {
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake));
                tabHome.setAnimation(s);
                getTabPos = 0;
                showFloatingActionButton();
            } else if (position == 1) {
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake));
                tabACS.setAnimation(s);
                getTabPos = 1;
                hideFloatingActionButton();
            } else {
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake));
                tabSNS.setAnimation(s);
                getTabPos = 2;
                hideFloatingActionButton();
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    public void SetFloatingActionButton() {
        getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        LayerDrawable layerDrawable = (LayerDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.floating_item_circle, null).getCurrent();
        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.gradientDrawble);
        gradientDrawable.setColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));

        itemBuilder = new SubActionButton.Builder(this);
        itemBuilder.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.floating_item_circle, null)).build();

        imgThemes = new ImageView(this);
        imgThemes.setImageResource(R.drawable.ic_bubble_chart_white_48dp);

        imgSettings = new ImageView(this);
        imgSettings.setImageResource(R.drawable.ic_settings_white_48dp);

        imgAbout = new ImageView(this);
        imgAbout.setImageResource(R.drawable.ic_info_outline_white_48dp);

        imgQuit = new ImageView(this);
        imgQuit.setImageResource(R.drawable.ic_exit_to_app_white_48dp);

        btnThemes = itemBuilder.setContentView(imgThemes).build();
        btnSettings = itemBuilder.setContentView(imgSettings).build();
        btnAbout = itemBuilder.setContentView(imgAbout).build();
        btnQuit = itemBuilder.setContentView(imgQuit).build();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        rightLowerMenu = new FloatingActionMenu.Builder(this)
                .setRadius((size.x/2) - (size.x/5))
                .enableAnimations()
                .setAnimationHandler(new DefaultAnimationHandler())
                .addSubActionView(btnQuit)
                .addSubActionView(btnAbout)
                .addSubActionView(btnSettings)
                .addSubActionView(btnThemes)
                .attachTo(fab)
                .build();

        rightLowerMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu menu) {
                fab.setRotation(0);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 180);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fab, pvhR);
                animation.start();
            }

            @Override
            public void onMenuClosed(FloatingActionMenu menu) {
                fab.setRotation(180);
                PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(fab, pvhR);
                animation.start();
            }
        });

        btnThemes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));
                s.addAnimation(EBIOTDefaultAnimationHandler.DisapperAnimation(2000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 2000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent (EBIOTMainActivity.this, EBIOTThemesActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));
                s.addAnimation(EBIOTDefaultAnimationHandler.DisapperAnimation(2000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 2000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent (EBIOTMainActivity.this, EBIOTSettingsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));
                s.addAnimation(EBIOTDefaultAnimationHandler.DisapperAnimation(2000, true));
                v.setAnimation(s);

                Thread animeThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            int waited = 0;

                            while (waited < 2000) {
                                sleep(50);
                                waited += 50;
                            }

                            Intent intent = new Intent(EBIOTMainActivity.this, EBIOTAboutActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                };
                animeThread.start();

                rightLowerMenu.close(true);
                hideFloatingActionButton();
            }
        });

        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet s = new AnimationSet(false);
                s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));
                s.addAnimation(EBIOTDefaultAnimationHandler.DisapperAnimation(2000, true));
                v.setAnimation(s);

                moveTaskToBack(true);

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    finishAffinity();
//                } else {
//                    finish();
//                }
            }
        });
    }

    public void showFloatingActionButton() {
        fab.show();
        fab.setVisibility(View.VISIBLE);
    }

    public void hideFloatingActionButton() {
        fab.hide();
        fab.setVisibility(View.GONE);
        rightLowerMenu.close(true);
    }

    void setUI () {
        getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        if (fab != null) {
            fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null)));
            fab.setRippleColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null));
            SetFloatingActionButton();
        }

        if (tabLayout != null) {
            tabLayout.setSelectedTabIndicatorColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorAccent(getThemes), null));
            tabLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        if (frameLayout != null) {
            frameLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }
    }

    @Override
    public void onStart () {
        super.onStart();
        setUI();

        if (getTabPos == 0) {
            showFloatingActionButton();
        } else {
            hideFloatingActionButton();
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        setUI();

        if (getTabPos == 0) {
            showFloatingActionButton();
        } else {
            hideFloatingActionButton();
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        EBIOTNotificationUtils.clearNotifications(this);

//        // register GCM registration complete receiver
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(EBIOTConfig.REGISTRATION_COMPLETE));
//
//        // register new push message receiver
//        // by doing this, the activity will be notified each time a new message arrives
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(EBIOTConfig.PUSH_NOTIFICATION));
//
//        // clear the notification area when the app is opened
//        EBIOTNotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    public void onPause () {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        hideFloatingActionButton();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideFloatingActionButton();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideFloatingActionButton();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
