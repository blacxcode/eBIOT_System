package com.blacxcode.ebiotdev.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTDefaultAnimationHandler;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.firebase.messaging.FirebaseMessaging;
import com.liuguangqiang.swipeback.SwipeBackLayout;

public class EBIOTThemesActivity extends AppCompatActivity {
    protected BroadcastReceiver mRegistrationBroadcastReceiver;
    protected SharedPreferences sharedPreferences;
    protected TextView themesTitle;
    protected FloatingActionButton setColor1, setColor2, setColor3, setColor4, setColor5,
        setColor6, setColor7, setColor8, setColor9, setColor10, setColor11, setColor12,
        setColor13, setColor14;
    protected SwipeBackLayout swipeBackLayout;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.TRANSPARENT);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);

        //setDragEdge(SwipeBackLayout.DragEdge.LEFT);
        swipeBackLayout = findViewById(R.id.swipeBackLayout);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);

        themesTitle = findViewById(R.id.themes_title);

        setColor1 = findViewById(R.id.set_color1);
        setColor2 = findViewById(R.id.set_color2);
        setColor3 = findViewById(R.id.set_color3);
        setColor4 = findViewById(R.id.set_color4);
        setColor5 = findViewById(R.id.set_color5);
        setColor6 = findViewById(R.id.set_color6);
        setColor7 = findViewById(R.id.set_color7);
        setColor8 = findViewById(R.id.set_color8);
        setColor9 = findViewById(R.id.set_color9);
        setColor10 = findViewById(R.id.set_color10);
        setColor11 = findViewById(R.id.set_color11);
        setColor12 = findViewById(R.id.set_color12);
        setColor13 = findViewById(R.id.set_color13);
        setColor14 = findViewById(R.id.set_color14);

        setUI();

        setColor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorPink");
                clearCheck();
                setColor1.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorPurple");
                clearCheck();
                setColor2.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorDeepPurple");
                clearCheck();
                setColor3.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorBlue");
                clearCheck();
                setColor4.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorLightBlue");
                clearCheck();
                setColor5.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorCyan");
                clearCheck();
                setColor6.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorLightGreen");
                clearCheck();
                setColor7.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorLime");
                clearCheck();
                setColor8.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorGrey");
                clearCheck();
                setColor9.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorBlueGrey");
                clearCheck();
                setColor10.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorBrown");
                clearCheck();
                setColor11.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorTeal");
                clearCheck();
                setColor12.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorIndigo");
                clearCheck();
                setColor13.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        setColor14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setColor("EBIOTColorRed");
                clearCheck();
                setColor14.setImageResource(R.drawable.ic_check_white_48dp);
                runAnim(view);
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(EBIOTConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(EBIOTConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(EBIOTConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");

                    Intent notify = new Intent (EBIOTThemesActivity.this, EBIOTNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    startActivity(notify);
                }
            }
        };
    }

    public void runAnim (View view) {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
        s.addAnimation(EBIOTDefaultAnimationHandler.DisapperAnimation(1000, true));
        view.setAnimation(s);

        Thread animeThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;

                    while (waited < 2000) {
                        sleep(50);
                        waited += 50;
                    }
                    Intent intent = new Intent (EBIOTThemesActivity.this, EBIOTMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    // do nothing
                }
            }
        };
        animeThread.start();
        themesTitle.setText(getResources().getString(R.string.themes_choose));
        themesTitle.setTextColor(ContextCompat.getColor(this, EBIOTSetColorTheme.getColorPrimaryDark(getThemes)));
    }

    public void setUI() {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        //themesTitle.setBackgroundColor(ContextCompat.getColor(this, EBIOTSetColorTheme.getColorPrimary(getThemes)));
        themesTitle.setTextColor(ContextCompat.getColor(this, EBIOTSetColorTheme.getColorPrimaryDark(getThemes)));
        themesTitle.setText(getResources().getString(R.string.themes_title));

        switch (getThemes) {
            case "EBIOTColorPink" :
                setColor1.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorPurple" :
                setColor2.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorDeepPurple" :
                setColor3.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorBlue" :
                setColor4.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorLightBlue" :
                setColor5.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorCyan" :
                setColor6.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorLightGreen" :
                setColor7.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorLime" :
                setColor8.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorGrey" :
                setColor9.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorBlueGrey" :
                setColor10.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorBrown" :
                setColor11.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorTeal" :
                setColor12.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorIndigo" :
                setColor13.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            case "EBIOTColorRed" :
                setColor14.setImageResource(R.drawable.ic_check_white_48dp);
                break;
            default:
        }
    }

    public void clearCheck() {
        setColor1.setImageResource(android.R.color.transparent);
        setColor2.setImageResource(android.R.color.transparent);
        setColor3.setImageResource(android.R.color.transparent);
        setColor4.setImageResource(android.R.color.transparent);
        setColor5.setImageResource(android.R.color.transparent);
        setColor6.setImageResource(android.R.color.transparent);
        setColor7.setImageResource(android.R.color.transparent);
        setColor8.setImageResource(android.R.color.transparent);
        setColor9.setImageResource(android.R.color.transparent);
        setColor10.setImageResource(android.R.color.transparent);
        setColor11.setImageResource(android.R.color.transparent);
        setColor12.setImageResource(android.R.color.transparent);
        setColor13.setImageResource(android.R.color.transparent);
        setColor14.setImageResource(android.R.color.transparent);
    }

    @Override
    public void onStart () {
        super.onStart();
        setUI();
    }

    @Override
    public void onResume () {
        super.onResume();
        setUI();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        EBIOTNotificationUtils.clearNotifications(this);
    }

    @Override
    public void onPause () {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
    }

    private void setColor(String setColor) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("EBIOTThemes", setColor);
        edit.apply();
    }
}
