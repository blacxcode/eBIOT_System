package com.blacxcode.ebiotdev.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.fragment.EBIOTSettingsFragment;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;


public class EBIOTSettingsActivity extends AppCompatActivity {
    protected BroadcastReceiver mRegistrationBroadcastReceiver;
    protected static final String TAG_SETTINGS = "EBIOTSettings";
    protected SharedPreferences sharedPreferences;
    protected Toolbar toolbar;
    protected InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        toolbar = findViewById(R.id.toolbar_settings_act);
        setSupportActionBar(toolbar);
        setUI ();

        //mInterstitialAd = newInterstitialAd();
        //loadInterstitial();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(EBIOTConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(EBIOTConfig.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(EBIOTConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String title = intent.getStringExtra("title");
                    String message = intent.getStringExtra("message");

                    Intent notify = new Intent (EBIOTSettingsActivity.this, EBIOTNotifyActivity.class);
                    notify.putExtra("title", title);
                    notify.putExtra("message", message);
                    startActivity(notify);
                }
            }
        };

    }

    void setUI () {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        LoadUISettings();
    }

    void LoadUISettings() {
        EBIOTSettingsFragment fragment = new EBIOTSettingsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame_layout_setting, fragment, TAG_SETTINGS);
        //fragmentTransaction.addToBackStack(CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
        setToolbarTitle(getResources().getString(R.string.activity_title_settings));
    }

    private void setToolbarTitle(String titleName) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (actionBar != null) {
            actionBar.setTitle(titleName);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null)));
        }
    }

    private void loadInterstitial() {
        final AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("15C0119EA7504242")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.i("showInterstitial", "Ad did not load");
            //Toast.makeText(getApplicationContext(), "Ad did not load", Toast.LENGTH_SHORT).show();
        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
                Log.i("onAdLoaded", "Ad loaded.");
                //Toast.makeText(getApplicationContext(), "Ad loaded.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Log.i("onAdFailedToLoad", "Ad failed to load with error code : " + errorCode);
                //Toast.makeText(getApplicationContext(), "Ad failed to load with error code : " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.i("onAdOpened", "Ad opened.");
                //Toast.makeText(getApplicationContext(), "Ad opened.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                showInterstitial();
                Log.i("onAdClosed", "Ad closed.");
                //Toast.makeText(getApplicationContext(), "Ad closed.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.i("onAdLeftApplication", "Ad left application.");
                //Toast.makeText(getApplicationContext(), "Ad left application.", Toast.LENGTH_SHORT).show();
            }
        });
        return interstitialAd;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart () {
        //Toast.makeText(getApplicationContext(), "settings onStart", Toast.LENGTH_SHORT).show();
        super.onStart();
        setUI();
    }

    @Override
    public void onResume () {
        //Toast.makeText(getApplicationContext(), "settings onResume", Toast.LENGTH_SHORT).show();
        super.onResume();
        setUI();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(EBIOTConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        EBIOTNotificationUtils.clearNotifications(this);
    }

    @Override
    public void onPause () {
        //Toast.makeText(getApplicationContext(), "settings onPause", Toast.LENGTH_SHORT).show();
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        //Toast.makeText(getApplicationContext(), "settings onStop", Toast.LENGTH_SHORT).show();
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(getApplicationContext(), "settings onDestroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}