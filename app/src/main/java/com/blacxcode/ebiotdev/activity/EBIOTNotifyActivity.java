package com.blacxcode.ebiotdev.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTRingtonesService;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;

import java.util.ArrayList;
import java.util.Locale;

import static android.speech.tts.TextToSpeech.Engine.ACTION_CHECK_TTS_DATA;

/**
 * Created by blacXcode on 5/28/2017.
 */

public class EBIOTNotifyActivity extends AppCompatActivity {
    protected SharedPreferences sharedPreferences;

    protected ImageButton ttsPlay, hideMe;
    protected Thread soundTimer, hideTimer;
    protected TextView headerMessage, pushMessage, langDetection;
    protected Locale setLang;
    protected TextToSpeech tts;
    protected AudioManager audioManager;
    protected CardView alertCardview;
    protected View view;

    private String detectedLanguage = "id";
    private int volume = 0;
    private float getSpeechrate = 0.0f, getPitch = 0.0f;
    private String getTitle = "", getMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
        view = findViewById(R.id.LinearLayout);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            getTitle = bundle.getString("title")  + getResources().getString(R.string.says);
            getMessage = bundle.getString("message");
        }

        headerMessage = findViewById(R.id.headerMessage);
        pushMessage = findViewById(R.id.pushMessage);
        langDetection = findViewById(R.id.langDetect);
        ttsPlay = findViewById(R.id.ttsPlay);
        hideMe = findViewById(R.id.hideMe);
        alertCardview = findViewById(R.id.alertCardview);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        alertCardview.setVisibility(View.VISIBLE);
        alertCardview.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.home_alert_zoom_out));

        headerMessage.setText(getTitle);

//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            detectLanguage(Html.fromHtml(getMessage,Html.FROM_HTML_MODE_LEGACY).toString());
//        } else {
//            detectLanguage(Html.fromHtml(getMessage).toString());
//        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            pushMessage.setText(Html.fromHtml(getMessage,Html.FROM_HTML_MODE_LEGACY));
        } else {
            pushMessage.setText(Html.fromHtml(getMessage));
        }

        ttsPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EBIOTNotificationUtils notificationUtils = new EBIOTNotificationUtils(getApplicationContext());
                notificationUtils.stopNotificationSound();

                isPlaying();
                speak(pushMessage.getText().toString());
            }
        });

        hideMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeNotif();

                alertCardview.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.home_alert_zoom_in));
                hideTimer = new Thread() {
                    @Override
                    public void run() {
                        //do something
                        int time = 1000;    //in milissegunds

                        try {
                            Thread.sleep(time);
                        } catch (Exception e) {
                            Log.e("Runnable","{0} exception caught.", e);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                                    try {
                                        alertCardview.setVisibility(View.GONE);

                                        if (hideTimer != null) {
                                            hideTimer.interrupt();
                                        }

                                        onBackPressed();
                                    } catch (Exception e) {
                                        Log.e("Runnable","{0} exception caught.", e);
                                    }
                                }
                            }
                        });
                    }
                };
                hideTimer.start();
            }
        });

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR || status == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void closeNotif () {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("EBIOTNOTIF", false);
        edit.apply();

        if (tts.isSpeaking()) {
            tts.stop();
        }

        EBIOTRingtonesService ringtonesService = new EBIOTRingtonesService(getApplicationContext());
        ringtonesService.stopRingtones();

        if (soundTimer != null) {
            soundTimer.interrupt();
        }

        EBIOTNotificationUtils notificationUtils = new EBIOTNotificationUtils(getApplicationContext());
        notificationUtils.stopNotificationSound();
    }

//    private void detectLanguage(final String source) {
//        final Handler handler = new Handler();
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                TranslateOptions options = TranslateOptions.newBuilder()
//                        .setApiKey(getResources().getString(R.string.api_key))
//                        .build();
//                Translate translate = options.getService();
//                final Detection detection = translate.detect(source);
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        detectedLanguage = detection.getLanguage();
//                        setLang = new Locale(detectedLanguage);
//                        langDetection.setText(setLang.getDisplayLanguage() + " (" + setLang.getLanguage() + ")");
//                    }
//                });
//                return null;
//            }
//        }.execute();
//    }

    private void isPlaying() {
        soundTimer = new Thread() {
            @Override
            public void run() {
                //do something
                int time = 500;    //in milissegunds

                try {
                    Thread.sleep(time);
                } catch (Exception e) {
                    Log.e("Runnable","{0} exception caught.", e);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                            try {
                                if (tts.isSpeaking()) {
                                    ttsPlay.setSelected(true);
                                } else {
                                    ttsPlay.setSelected(false);

                                    if (soundTimer != null) {
                                        soundTimer.interrupt();
                                    }
                                }
                                ttsPlay.refreshDrawableState();
                            } catch (Exception e) {
                                Log.e("Runnable","{0} exception caught.", e);
                            }
                        }
                        isPlaying();
                    }
                });
            }
        };
        soundTimer.start();
    }

    @SuppressLint("Deprecation")
    private void speak(String stts) {
        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);

        float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
        float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

        setLang = new Locale(detectedLanguage);
        installVoiceData();
        tts.setLanguage(setLang);
        tts.setPitch(pitch);
        tts.setSpeechRate(speechrate);

        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

        if (tts.isSpeaking()) {
            tts.stop();
            if (soundTimer != null) {
                soundTimer.interrupt();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    private void installVoiceData() {
        int availability = tts.isLanguageAvailable(setLang);
        switch (availability) {
            case TextToSpeech.LANG_COUNTRY_AVAILABLE :
            case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:
            case TextToSpeech.LANG_AVAILABLE :
                Snackbar.make(view, getResources().getString(R.string.ttsLangAvailable), Snackbar.LENGTH_SHORT).show();
                break;
            case TextToSpeech.LANG_NOT_SUPPORTED :
                Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_SHORT).show();

                Intent intent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                ArrayList<String> languages = new ArrayList<>();

                languages.add(detectedLanguage);
                intent.putStringArrayListExtra(ACTION_CHECK_TTS_DATA, languages);
                startActivity(intent);
                break;
            case TextToSpeech.LANG_MISSING_DATA :
                Snackbar.make(view, getResources().getString(R.string.ttsLangMissingData), Snackbar.LENGTH_SHORT).show();
                break;
            default:
        }
    }

    public void setUI() {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
            window.setNavigationBarColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimaryDark(getThemes), null));
        }

        alertCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));

        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);
        int getVolume = sharedPreferences.getInt("EBIOTVolume", 8);

        volume = getVolume/10;
    }

    @Override
    public void onStart () {
        super.onStart();
        setUI();
    }

    @Override
    public void onResume () {
        super.onResume();
        setUI();
    }

    @Override
    public void onPause () {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        closeNotif();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
    }
}
