package com.blacxcode.ebiotdev.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by blacXcode on 10/28/2016.
 *
 * Copyright (C) 2016 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

public class EBIOTCoreListenerFragment extends Fragment {
    protected SharedPreferences sharedPreferences;

    protected Context context;
    protected AdView mAdView;
    protected View view;

    public static EBIOTCoreListenerFragment newInstance() {
        return new EBIOTCoreListenerFragment();
    }

    public EBIOTCoreListenerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_core_listener, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdView = view.findViewById(R.id.listener_av_main);

        mAdView.setAdListener(new AdListener() {
            private void showToast(String message) {
                View view = getView();
                if (view != null) {
                    Toast.makeText(getView().getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAdLoaded() {
                //showToast("Ad loaded.");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //showToast(String.format(Locale.getDefault(), "Ad failed to load with error code %d.", errorCode));
            }

            @Override
            public void onAdOpened() {
                //showToast("Ad opened.");
                //hide action button
            }

            @Override
            public void onAdClosed() {
                //showToast("Ad closed.");
            }

            @Override
            public void onAdLeftApplication() {
                //showToast("Ad left application.");
            }
        });

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("15C0119EA7504242")
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onStart () {
        super.onStart();
    }

    @Override
    public void onResume () {
        super.onResume();
    }

    @Override
    public void onPause () {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
