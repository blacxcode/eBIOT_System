package com.blacxcode.ebiotdev.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.activity.EBIOTMainActivity;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTRingtonesService;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static android.speech.tts.TextToSpeech.Engine.ACTION_CHECK_TTS_DATA;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class EBIOTHomeFragment extends Fragment {
    private static final String TAG = EBIOTMainActivity.class.getSimpleName();

    protected BroadcastReceiver mRegistrationBroadcastReceiver;
    protected TextView regID, headerMessage, pushMessage, langDetection;

    protected SharedPreferences sharedPreferences;

    protected FrameLayout frameLayout;
    protected Context context;
    protected View view;
    protected CardView alertCardview, registCardview;
    protected ImageButton ttsPlay, hideMe;
    protected Locale setLang;
    protected TextToSpeech tts;
    protected AudioManager audioManager;
    protected Thread soundTimer, hideTimer;

    private String detectedLanguage = "id";
    private int volume = 0;
    private float getSpeechrate = 0.0f, getPitch = 0.0f;

    public EBIOTHomeFragment() {
        // Required empty public constructor
    }

    public static EBIOTHomeFragment newInstance() {
        return new EBIOTHomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, parent, false);

        frameLayout = view.findViewById(R.id.homeFragment);
        regID = view.findViewById(R.id.regID);
        headerMessage = view.findViewById(R.id.headerMessage);
        pushMessage = view.findViewById(R.id.pushMessage);
        langDetection = view.findViewById(R.id.langDetect);
        registCardview = view.findViewById(R.id.registCardview);
        alertCardview = view.findViewById(R.id.alertCardview);

        ttsPlay = view.findViewById(R.id.ttsPlay);
        hideMe = view.findViewById(R.id.hideMe);

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        setUI();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String getTitle = "", getMessage = "";

        Bundle bundle = getActivity().getIntent().getExtras();

        if (bundle != null) {
            getTitle = bundle.getString("title") + getResources().getString(R.string.says);
            getMessage = bundle.getString("message");

            alertCardview.setVisibility(View.VISIBLE);
            alertCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.home_alert_zoom_out));

            headerMessage.setText(getTitle);

//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                detectLanguage(Html.fromHtml(getMessage,Html.FROM_HTML_MODE_LEGACY).toString());
//            } else {
//                detectLanguage(Html.fromHtml(getMessage).toString());
//            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                pushMessage.setText(Html.fromHtml(getMessage,Html.FROM_HTML_MODE_LEGACY));
            } else {
                pushMessage.setText(Html.fromHtml(getMessage));
            }
        }

        displayFirebaseRegId();

        registCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationSet animationSet = new AnimationSet(true);//false means don't share interpolators
                animationSet.addAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_360));
                animationSet.addAnimation(AnimationUtils.loadAnimation(context, R.anim.show_up));

                if (regID.getText().equals(getResources().getString(R.string.regDevice))) {
                    String devicekey = sharedPreferences.getString("devicekey", "");
                    registCardview.startAnimation(animationSet);
                    regID.setText(devicekey);
                } else if (regID.getText().equals(getResources().getString(R.string.notregDevice))) {
                    regID.setText(R.string.notregDevice);
                } else {
                    registCardview.startAnimation(animationSet);
                    regID.setText(getResources().getString(R.string.regDevice));
                }
            }
        });

        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR || status == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        ttsPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EBIOTNotificationUtils notificationUtils = new EBIOTNotificationUtils(context);
                notificationUtils.stopNotificationSound();

                isPlaying();
                speak(pushMessage.getText().toString());
            }
        });

        hideMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeNotif();

                alertCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.home_alert_zoom_in));
                hideTimer = new Thread() {
                    @Override
                    public void run() {
                        //do something
                        int time = 1000;    //in milissegunds

                        try {
                            Thread.sleep(time);
                        } catch (Exception e) {
                            Log.e("Runnable","{0} exception caught.", e);
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                                    try {
                                        alertCardview.setVisibility(View.GONE);

                                        if (hideTimer != null) {
                                            hideTimer.interrupt();
                                        }
                                    } catch (Exception e) {
                                        Log.e("Runnable","{0} exception caught.", e);
                                    }
                                }
                            }
                        });
                    }
                };
                hideTimer.start();
            }
        });
    }

    private void displayFirebaseRegId() {
        String devicekey = sharedPreferences.getString("devicekey", "");

        Log.e(TAG, "Firebase Reg ID: " + devicekey);

        if (!TextUtils.isEmpty(devicekey)) {
            regID.setText(getResources().getString(R.string.regDevice));
        } else {
            regID.setText(getResources().getString(R.string.notregDevice));
        }
    }

    private void closeNotif() {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("EBIOTNOTIF", false);
        edit.apply();

        if (tts.isSpeaking()) {
            tts.stop();
        }

        EBIOTRingtonesService ringtonesService = new EBIOTRingtonesService(context);
        ringtonesService.stopRingtones();

        if (soundTimer != null) {
            soundTimer.interrupt();
        }

        EBIOTNotificationUtils notificationUtils = new EBIOTNotificationUtils(context);
        notificationUtils.stopNotificationSound();
    }

//    private void detectLanguage(final String source) {
//        final Handler handler = new Handler();
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                TranslateOptions options = TranslateOptions.newBuilder()
//                        .setApiKey(getResources().getString(R.string.api_key))
//                        .build();
//                Translate translate = options.getService();
//                final Detection detection = translate.detect(source);
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        detectedLanguage = detection.getLanguage();
//                        setLang = new Locale(detectedLanguage);
//                        langDetection.setText(setLang.getDisplayLanguage() + " (" + setLang.getLanguage() + ")");
//                    }
//                });
//                return null;
//            }
//        }.execute();
//    }

    @SuppressLint("Deprecation")
    private void speak(String stts) {
        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);

        float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
        float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

        setLang = new Locale(detectedLanguage);
        installVoiceData();
        tts.setLanguage(setLang);
        tts.setPitch(pitch);
        tts.setSpeechRate(speechrate);

        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

        if (tts.isSpeaking()) {
            tts.stop();
            if (soundTimer != null) {
                soundTimer.interrupt();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    private void installVoiceData() {
        int availability = tts.isLanguageAvailable(setLang);
        switch (availability) {
            case TextToSpeech.LANG_COUNTRY_AVAILABLE :
            case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:
            case TextToSpeech.LANG_AVAILABLE :
                Snackbar.make(view, getResources().getString(R.string.ttsLangAvailable), Snackbar.LENGTH_SHORT).show();
                break;
            case TextToSpeech.LANG_NOT_SUPPORTED :
                Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_SHORT).show();

                Intent intent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                ArrayList<String> languages = new ArrayList<>();

                languages.add(detectedLanguage);
                intent.putStringArrayListExtra(ACTION_CHECK_TTS_DATA, languages);
                startActivity(intent);
                break;
            case TextToSpeech.LANG_MISSING_DATA :
                Snackbar.make(view, getResources().getString(R.string.ttsLangMissingData), Snackbar.LENGTH_SHORT).show();
                break;
            default:
        }
    }

    private void isPlaying() {
        soundTimer = new Thread() {
            @Override
            public void run() {
                //do something
                int time = 500;    //in milissegunds

                try {
                    Thread.sleep(time);
                } catch (Exception e) {
                    Log.e("Runnable","{0} exception caught.", e);
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                            try {
                                if (tts.isSpeaking()) {
                                    ttsPlay.setSelected(true);
                                } else {
                                    ttsPlay.setSelected(false);

                                    if (soundTimer != null) {
                                        soundTimer.interrupt();
                                    }
                                }
                                ttsPlay.refreshDrawableState();
                            } catch (Exception e) {
                                Log.e("Runnable","{0} exception caught.", e);
                            }
                        }
                        isPlaying();
                    }
                });
            }
        };
        soundTimer.start();
    }

    void setUI() {
        String getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        frameLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));
        registCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        alertCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));

        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);
        int getVolume = sharedPreferences.getInt("EBIOTVolume", 8);

        volume = getVolume/10;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
