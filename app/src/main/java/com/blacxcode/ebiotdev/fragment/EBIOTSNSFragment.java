package com.blacxcode.ebiotdev.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.activity.EBIOTMainActivity;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.sync.EBIOTDBParseData;
import com.blacxcode.ebiotdev.util.EBIOTCircleTransform;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class EBIOTSNSFragment extends Fragment {
    private static final String TAG = EBIOTMainActivity.class.getSimpleName();
    private final String PENDING_ACTION_BUNDLE_KEY = "com.blacxcode.ebiotdev:PendingAction";

    protected SharedPreferences sharedPreferences;

    // Facebook
    protected LoginButton loginButton;
    protected Button postStatusUpdateButton;
    protected Button requestAccessButton;
    protected PendingAction pendingAction = PendingAction.NONE;
    protected CallbackManager callbackManager;
    protected ProfileTracker profileTracker;
    protected ShareDialog shareDialog;
    //private TextView spaceLogin;
    protected TextView greeting;
    protected ImageView profilePicImageView;

    protected Context context;
    protected View view;

    protected EBIOTDBParseData dbParseData;
    protected DatabaseReference mFirebaseDBClient;
    protected FirebaseDatabase mFirebaseInstance;

    //private static final String TAG_GOOGLEPLUS = "googleplus";
    private static final String TAG_FACEBOOK = "facebook";
    private String isState = null;
    private boolean isFacebook = false;
    //private boolean isGoogle = false;
    //private boolean isGoogleSignin = false;
    //private String imgProfileURL;


    // Facebook
    private boolean postingEnabled = false;
    private boolean canPresentShareDialog;

    // Google
    //private SignInButton signInButton;
    //private Button signOutButton;
    //private GoogleApiClient mGoogleApiClient;
    //private ProgressDialog mProgressDialog;
    //private int RC_SIGN_IN = 0;

    public EBIOTSNSFragment() {
        // Required empty public constructor
    }

    public static EBIOTSNSFragment newInstance() {
        return new EBIOTSNSFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);

        dbParseData = new EBIOTDBParseData();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDBClient = mFirebaseInstance.getReference(EBIOTConfig.DB_EBIOT);

        FacebookSdk.sdkInitialize(getActivity());
        // Other app specific specialization

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
//        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
//                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(ConnectionResult connectionResult) {
//                        Log.d(TAG_GOOGLEPLUS, "onConnectionFailed:" + connectionResult);
//                    }
//                })
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sns, parent, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // google
        //signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        //signOutButton = (Button) view.findViewById(R.id.sign_out_button);

        // facebook
        loginButton = view.findViewById(R.id.loginButton);
        postStatusUpdateButton = view.findViewById(R.id.postStatusUpdateButton);
        requestAccessButton = view.findViewById(R.id.requestAccessButton);
        // If using in a fragment
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, shareCallback);
        // Can we present the share dialog for regular links?
        canPresentShareDialog = ShareDialog.canShow(ShareLinkContent.class);

        profilePicImageView = view.findViewById(R.id.profilePicture);

        Glide.with(getActivity())
                .load(R.drawable.com_facebook_profile_picture_blank_square)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new EBIOTCircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profilePicImageView);

        //spaceLogin = view.findViewById(R.id.space_login);
        greeting = view.findViewById(R.id.greeting);

        greeting.setAlpha(0.9f);
        greeting.setTypeface(null, Typeface.NORMAL);

        // Google Activity
//        signInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GoogleSignIn();
//            }
//        });

//        signOutButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GoogleSignOut();
//            }
//        });

        // Facebook Activity
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                postingEnabled = true;
                postStatusUpdateButton.setVisibility(View.VISIBLE);
                requestAccessButton.setVisibility(View.VISIBLE);
                //signInButton.setVisibility(View.GONE);
                //signOutButton.setVisibility(View.GONE);
                //spaceLogin.setVisibility(View.GONE);

                handlePendingAction();

                //if (!isGoogle) {
                isState = TAG_FACEBOOK;
                updateUI();
                //}
            }

            @Override
            public void onCancel() {
                if (pendingAction != PendingAction.NONE) {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }

                //if (!isGoogle) {
                isState = TAG_FACEBOOK;
                updateUI();
                //}
            }

            @Override
            public void onError(FacebookException exception) {
                if (pendingAction != PendingAction.NONE && exception instanceof FacebookAuthorizationException) {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }

                //if (!isGoogle) {
                isState = TAG_FACEBOOK;
                updateUI();
                //}
            }

            private void showAlert() {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.cancelled)
                        .setMessage(R.string.permission_not_granted)
                        .setPositiveButton(R.string.ok, null)
                        .show();

            }
        });

        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

                //if (!isGoogle) {
                isState = TAG_FACEBOOK;
                updateUI();
                //}

                handlePendingAction();
            }
        };

        postStatusUpdateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onClickPostStatusUpdate();
            }
        });

        requestAccessButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (!sharedPreferences.getString("EBIOTHostname", "").equals("")) {
                        sendRequest();
                        requestAccessButton.setEnabled(false);
                        requestAccessButton.setText(getResources().getString(R.string.waiting_request));
                    } else {
                        Snackbar.make(view, getResources().getString(R.string.no_hostname), Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> permissionNeeds = Arrays.asList("public_profile", "user_photos", "friends_photos", "email", "user_birthday", "user_friends");
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), permissionNeeds);

                if (!postingEnabled) {
                    postingEnabled = true;
                    postStatusUpdateButton.setVisibility(View.VISIBLE);
                    requestAccessButton.setVisibility(View.VISIBLE);

                    requestAccess();

                } else {
                    postingEnabled = false;
                    postStatusUpdateButton.setVisibility(View.GONE);
                    requestAccessButton.setVisibility(View.GONE);
                }

                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        if (object != null) {
                            Log.d("Me Request", object.toString());

                        }

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,cover_id,source");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends"));
            }
        });

        requestAccess();
    }

    @Override
    public void onStart() {
        super.onStart();
        requestAccess();
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d(TAG_GOOGLEPLUS, "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
//            //showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
//                    //hideProgressDialog();
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.activateApp(getActivity());

        //String getSession = sharedPreferences.getString("EBIOTLoginSession", TAG_GOOGLEPLUS);
        //if (getSession.compareTo(TAG_FACEBOOK) == 0) {
        isState = TAG_FACEBOOK;
        //}

        //if (getSession.compareTo(TAG_GOOGLEPLUS) == 0) {
        //    isState = TAG_GOOGLEPLUS;
        //}

        updateUI();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(getActivity());

//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.stopAutoManage(getActivity());
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.stopAutoManage(getActivity());
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            profileTracker.stopTracking();
        } catch (Exception e) {
            Log.e("profileTracker", e.toString());
        }

//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.stopAutoManage(getActivity());
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        }
    }

    private void updateUI() {

        if ((isState.compareTo(TAG_FACEBOOK) == 0) /*&& (isState.compareTo(TAG_GOOGLEPLUS) != 0)*/) {
            boolean enableButtons = AccessToken.getCurrentAccessToken() != null;
            postStatusUpdateButton.setEnabled(enableButtons || canPresentShareDialog);

            Profile profile = Profile.getCurrentProfile();
            if (enableButtons && profile != null) {
                Glide.with(getActivity())
                        .load(profile.getProfilePictureUri(200, 200).toString())
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new EBIOTCircleTransform(context))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profilePicImageView);

                setGreeting(profile.getFirstName());

                String name = profile.getFirstName();
                greeting.setText(getString(R.string.hello_user, name));
                //String nav_greeting = getString(R.string.hello_user, profile.getFirstName()).concat(getString(R.string.greeting));

                setFlagOnline(true);

                postingEnabled = true;
                postStatusUpdateButton.setVisibility(View.VISIBLE);
                requestAccessButton.setVisibility(View.VISIBLE);
                //signInButton.setVisibility(View.GONE);
                //signOutButton.setVisibility(View.GONE);
                //spaceLogin.setVisibility(View.GONE);

                requestAccess();

                isFacebook = true;

                SharedPreferences.Editor logs = sharedPreferences.edit();
                logs.putString("EBIOTLoginSession", TAG_FACEBOOK);
                logs.apply();
            } else {
                if (isFacebook) {
                    logOut();
                }

                isFacebook = false;
            }
        }

//        if ((isState.compareTo(TAG_GOOGLEPLUS) == 0) && (isState.compareTo(TAG_FACEBOOK) != 0)) {
//            if (isGoogleSignin) {
//                loginButton.setVisibility(View.GONE);
//                postingEnabled = false;
//                postStatusUpdateButton.setVisibility(View.GONE);
//                requestAccessButton.setVisibility(View.GONE);
//                spaceLogin.setVisibility(View.GONE);
//                signInButton.setVisibility(View.GONE);
//                signOutButton.setVisibility(View.VISIBLE);
//                isGoogle = true;
//            } else {
//                if(isGoogle) {
//                    logOut();
//                }
//
//                isGoogle = false;
//
//                SharedPreferences.Editor logs = sharedPreferences.edit();
//                logs.putString("EBIOTLoginSession", TAG_GOOGLEPLUS);
//                logs.apply();
//            }
//        }

        if (/*(isState.compareTo(TAG_GOOGLEPLUS) != 0) &&*/ (isState.compareTo(TAG_FACEBOOK) != 0)) {
            logOut();
        }
    }

    private void requestAccess() {
        if (sharedPreferences.getString("remoteallow", "").equals("true")) {
            requestAccessButton.setEnabled(false);
            requestAccessButton.setText(getResources().getString(R.string.request_approved));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("remoteallow", "true");
            edit.apply();
        } else if (sharedPreferences.getString("remoteallow", "").equals("request")) {
            requestAccessButton.setEnabled(false);
            requestAccessButton.setText(getResources().getString(R.string.waiting_request));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("remoteallow", "request");
            edit.apply();
        } else {
            requestAccessButton.setEnabled(true);
            requestAccessButton.setText(getResources().getString(R.string.request_access));

            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("remoteallow", "false");
            edit.apply();
        }
    }

    private void logOut() {
        Glide.with(getActivity())
                .load(R.drawable.com_facebook_profile_picture_blank_square)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new EBIOTCircleTransform(context))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profilePicImageView);

        greeting.setText(getResources().getString(R.string.login_greeting));
        greeting.setAlpha(0.5f);
        greeting.setTypeface(null, Typeface.ITALIC);

        setGreeting(null);

        setFlagOnline(false);

        loginButton.setVisibility(View.VISIBLE);
        postingEnabled = false;
        postStatusUpdateButton.setVisibility(View.GONE);
        requestAccessButton.setVisibility(View.GONE);
        //spaceLogin.setVisibility(View.VISIBLE);
        //signInButton.setVisibility(View.VISIBLE);
        //signOutButton.setVisibility(View.GONE);
    }

    // [START facebook Config]
    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d(TAG_FACEBOOK, "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d(TAG_FACEBOOK, String.format("Error: %s", error.toString()));
            String title = getString(R.string.error);
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d(TAG_FACEBOOK, "Success!");
            if (result.getPostId() != null) {
                String title = getString(R.string.success);
                String alertMessage = getString(R.string.successfully_posted_post);
                showResult(title, alertMessage);
            }
        }

        private void showResult(String title, String alertMessage) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        }
    };

    private enum PendingAction {
        NONE,
        POST_STATUS_UPDATE
    }

    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_STATUS_UPDATE:
                postStatusUpdate();
                break;
        }
    }

    private void onClickPostStatusUpdate() {
        performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
    }

    private void postStatusUpdate() {
        Profile profile = Profile.getCurrentProfile();
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setQuote("Get apps eBIOT System in playstore")
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.blacxcode.ebiotdev"))
                .build();
        if (canPresentShareDialog) {
            shareDialog.show(linkContent);
        } else if (profile != null && hasPublishPermission()) {
            ShareApi.share(linkContent, shareCallback);
        } else {
            pendingAction = PendingAction.POST_STATUS_UPDATE;
        }
    }

    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }

    private void performPublish(PendingAction action, boolean allowNoToken) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null || allowNoToken) {
            pendingAction = action;
            handlePendingAction();
        }
    }
    // [END facebook Config]

    // [START google Config]
//    private void handleSignInResult(GoogleSignInResult result) {
//        //Log.d(TAG, "handleSignInResult:" + result.isSuccess());
//        if (result.isSuccess()) {
//            // Signed in successfully, show authenticated UI.
//            GoogleSignInAccount acct = result.getSignInAccount();
//
//            //Similarly you can get the email and photourl using acct.getEmail() and  acct.getPhotoUrl()
//            if(acct.getPhotoUrl() != null) {
//                Glide.with(getActivity())
//                        .load(acct.getPhotoUrl().toString())
//                        .crossFade()
//                        .thumbnail(0.5f)
//                        .bitmapTransform(new EBIOTCircleTransform(context))
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .into(profilePicImageView);
//
//                setOnlineProfile(acct.getPhotoUrl().toString());
//            } else {
//                Glide.with(getActivity())
//                        .load(R.drawable.com_facebook_profile_picture_blank_square)
//                        .crossFade()
//                        .thumbnail(0.5f)
//                        .bitmapTransform(new EBIOTCircleTransform(context))
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .into(profilePicImageView);
//
//                setOnlineProfile(null);
//            }
//
//            if (acct.getDisplayName() != null) {
//                setGreeting(acct.getDisplayName());
//
//                String name = acct.getDisplayName();
//                greeting.setText(getString(R.string.hello_user, name));
//            }
//
//            setFlagOnline(true);
//
//            // true
//            if (!isFacebook) {
//                isState = TAG_GOOGLEPLUS;
//                isGoogleSignin = true;
//                updateUI();
//            }
//        } else {
//            // Signed out, show unauthenticated UI.
//
//            // false
//            if (!isFacebook) {
//                isState = TAG_GOOGLEPLUS;
//                isGoogleSignin = false;
//                updateUI();
//            }
//        }
//    }

//    private void GoogleSignIn() {
//        // Google sign in ...
//        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(intent, RC_SIGN_IN);
//    }

//    private void GoogleSignOut() {
//        // Google sign out ...
//        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//
//                        // false
//                        if (!isFacebook) {
//                            isState = TAG_GOOGLEPLUS;
//                            isGoogleSignin = false;
//                            updateUI();
//                        }
//                    }
//                }
//        );
//    }

//    private void GoogleRevokeAccess() {
//        // Google revoke access ...
//        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//
//                        // false
//                        if (!isFacebook) {
//                            isState = TAG_GOOGLEPLUS;
//                            isGoogleSignin = false;
//                            updateUI();
//                        }
//                    }
//                }
//        );
//    }

//    private void showProgressDialog() {
//        if (mProgressDialog == null) {
//            mProgressDialog = new ProgressDialog(getActivity());
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(true);
//        }
//
//        mProgressDialog.show();
//    }

//    private void hideProgressDialog() {
//        if (mProgressDialog != null && mProgressDialog.isShowing()) {
//            mProgressDialog.hide();
//        }
//    }

    public void setOnlineProfile(String setOnlineProfile) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("EBIOTSetOnlineProfile", setOnlineProfile);
        edit.apply();
    }

    public void setGreeting(String greeting) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("EBIOTSetGreeting", greeting);
        edit.apply();
    }

    public void setFlagOnline(boolean flagOnline) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("EBIOTSetFlagOnline", flagOnline);
        edit.apply();
    }

    public void sendRequest(){
        Profile profile = Profile.getCurrentProfile();

        String clientid = sharedPreferences.getString("clientid", "0");
        String deviceid = sharedPreferences.getString("deviceid", "");
        String devicekey = sharedPreferences.getString("devicekey", "");

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("remoteallow", "request");
        edit.apply();

        dbParseData.updateClient(mFirebaseDBClient, clientid, deviceid, devicekey, "request", profile.getFirstName());
    }
    // [END google Config]
}