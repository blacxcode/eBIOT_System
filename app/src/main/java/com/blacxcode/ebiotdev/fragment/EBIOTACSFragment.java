package com.blacxcode.ebiotdev.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.activity.EBIOTMainActivity;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.sync.EBIOTDBParseData;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class EBIOTACSFragment extends Fragment {
    private static final String TAG = EBIOTMainActivity.class.getSimpleName();

    protected SharedPreferences sharedPreferences;

    protected FrameLayout frameLayout;
    protected EBIOTDBParseData dbParseData;
    protected DatabaseReference mFirebaseDBData;
    protected FirebaseDatabase mFirebaseInstance;

    protected ToggleButton toggleButton1, toggleButton2, toggleButton3, toggleButton4, toggleButton5, toggleButton6, toggleButton7, toggleButton8;
    protected EditText  editText1, editText2, editText3, editText4, editText5, editText6, editText7, editText8;
    protected TextView textView;
    protected Context context;
    protected View view;
    protected CardView cardview;
    protected LinearLayout linearLayout;


    private String data[] = new String[40];
    private String getThemes = "EBIOTColorPink";

    public EBIOTACSFragment() {
        // Required empty public constructor
    }

    public static EBIOTACSFragment newInstance() {
        return new EBIOTACSFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        int getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_access_control, parent, false);

        frameLayout = view.findViewById(R.id.acsFragment);

        dbParseData = new EBIOTDBParseData();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDBData = mFirebaseInstance.getReference(EBIOTConfig.DB_EBIOT);

        cardview = view.findViewById(R.id.notAllow_cardview);
        linearLayout = view.findViewById(R.id.panelControl);

        toggleButton1 = view.findViewById(R.id.toggleButton1);
        toggleButton2 = view.findViewById(R.id.toggleButton2);
        toggleButton3 = view.findViewById(R.id.toggleButton3);
        toggleButton4 = view.findViewById(R.id.toggleButton4);
        toggleButton5 = view.findViewById(R.id.toggleButton5);
        toggleButton6 = view.findViewById(R.id.toggleButton6);
        toggleButton7 = view.findViewById(R.id.toggleButton7);
        toggleButton8 = view.findViewById(R.id.toggleButton8);

        editText1 = view.findViewById(R.id.EditText1);
        editText2 = view.findViewById(R.id.EditText2);
        editText3 = view.findViewById(R.id.EditText3);
        editText4 = view.findViewById(R.id.EditText4);
        editText5 = view.findViewById(R.id.EditText5);
        editText6 = view.findViewById(R.id.EditText6);
        editText7 = view.findViewById(R.id.EditText7);
        editText8 = view.findViewById(R.id.EditText8);

        textView = view.findViewById(R.id.notAllowText);
        textView.setText(getResources().getString(R.string.notAllow));

        convertBinary(0);
        setUI();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updatedData();

        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton1.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[0] = "1";
                        setACS();
                        toggleButton1.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton1.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton1.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }

                } else {
                    try {
                        data[0] = "0";
                        setACS();
                        toggleButton1.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton1.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton1.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle1", v.getText().toString());
                    edit.apply();

                    //.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton2.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[1] = "1";
                        setACS();
                        toggleButton2.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton2.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton2.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[1] = "0";
                        setACS();
                        toggleButton2.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton2.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton2.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle2", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton3.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[2] = "1";
                        setACS();
                        toggleButton3.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton3.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton3.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[2] = "0";
                        setACS();
                        toggleButton3.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton3.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton3.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText3.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle3", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton4.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[3] = "1";
                        setACS();
                        toggleButton4.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton4.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton4.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[3]= "0";
                        setACS();
                        toggleButton4.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton4.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton4.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle4", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton5.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[4] = "1";
                        setACS();
                        toggleButton5.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton5.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton5.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[4] = "0";
                        setACS();
                        toggleButton5.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton5.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton5.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText5.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle5", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton6.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[5] = "1";
                        setACS();
                        toggleButton6.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton6.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton6.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[5] = "0";
                        setACS();
                        toggleButton6.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton6.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton6.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText6.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle6", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton7.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[6] = "1";
                        setACS();
                        toggleButton7.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton7.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton7.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[6] = "0";
                        setACS();
                        toggleButton7.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton7.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton7.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText7.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle7", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        toggleButton8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleButton8.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                if (isChecked) {
                    try {
                        data[7] = "1";
                        setACS();
                        toggleButton8.setTextColor(Color.WHITE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton8.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                        } else {
                            toggleButton8.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        data[7] = "0";
                        setACS();
                        toggleButton8.setTextColor(Color.GRAY);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            toggleButton8.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                        } else {
                            toggleButton8.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                        }
                    } catch (Exception e) {
                        Snackbar.make(view, getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        editText8.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEND) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_GO)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTTitle8", v.getText().toString());
                    edit.apply();

                    //Toast.makeText(context, getResources().getString(R.string.nameDevice), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }

    public void setACS(){
        String pdata;
        int decimalValue;

        pdata = data[0] + data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7];
        decimalValue = Integer.valueOf(pdata, 2);

        dbParseData.setIOTDACS(mFirebaseDBData, String.valueOf(decimalValue));
        //Toast.makeText(context, "data : " + ebiotData.getIotdacs(), Toast.LENGTH_SHORT).show();
    }

    public void convertBinary(int num){
        int binary[] = new int[10];
        int index = 0;

        while(num > 0){
            binary[index++] = num%2;
            num = num/2;
        }

        data[0] = String.valueOf(binary[7]);
        Log.v(TAG, "binary[0] : " + binary[7]);
        data[1] = String.valueOf(binary[6]);
        Log.v(TAG, "binary[1] : " + binary[6]);
        data[2] = String.valueOf(binary[5]);
        Log.v(TAG, "binary[2] : " + binary[5]);
        data[3] = String.valueOf(binary[4]);
        Log.v(TAG, "binary[3] : " + binary[4]);
        data[4] = String.valueOf(binary[3]);
        Log.v(TAG, "binary[4] : " + binary[3]);
        data[5] = String.valueOf(binary[2]);
        Log.v(TAG, "binary[5] : " + binary[2]);
        data[6] = String.valueOf(binary[1]);
        Log.v(TAG, "binary[6] : " + binary[1]);
        data[7] = String.valueOf(binary[0]);
        Log.v(TAG, "binary[7] : " + binary[0]);
    }

    public void setUI() {
        getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        frameLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));

        if(data[0].equals("1")) {
            toggleButton1.setChecked(true);
            toggleButton1.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton1.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton1.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton1.setChecked(false);
            toggleButton1.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton1.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton1.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[1].equals("1")) {
            toggleButton2.setChecked(true);
            toggleButton2.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton2.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton2.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton2.setChecked(false);
            toggleButton2.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton2.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton2.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[2].equals("1")) {
            toggleButton3.setChecked(true);
            toggleButton3.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton3.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton3.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton3.setChecked(false);
            toggleButton3.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton3.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton3.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[3].equals("1")) {
            toggleButton4.setChecked(true);
            toggleButton4.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton4.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton4.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton4.setChecked(false);
            toggleButton4.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton4.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton4.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[4].equals("1")) {
            toggleButton5.setChecked(true);
            toggleButton5.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton5.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton5.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton5.setChecked(false);
            toggleButton5.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton5.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton5.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[5].equals("1")) {
            toggleButton6.setChecked(true);
            toggleButton6.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton6.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton6.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton6.setChecked(false);
            toggleButton6.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton6.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton6.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[6].equals("1")) {
            toggleButton7.setChecked(true);
            toggleButton7.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton7.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton7.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton7.setChecked(false);
            toggleButton7.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton7.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton7.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        if(data[7].equals("1")) {
            toggleButton8.setChecked(true);
            toggleButton8.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton8.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                toggleButton8.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            toggleButton8.setChecked(false);
            toggleButton8.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toggleButton8.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                toggleButton8.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        editText1.setText(sharedPreferences.getString("EBIOTTitle1", ""));
        editText2.setText(sharedPreferences.getString("EBIOTTitle2", ""));
        editText3.setText(sharedPreferences.getString("EBIOTTitle3", ""));
        editText4.setText(sharedPreferences.getString("EBIOTTitle4", ""));
        editText5.setText(sharedPreferences.getString("EBIOTTitle5", ""));
        editText6.setText(sharedPreferences.getString("EBIOTTitle6", ""));
        editText7.setText(sharedPreferences.getString("EBIOTTitle7", ""));
        editText8.setText(sharedPreferences.getString("EBIOTTitle8", ""));

        if (sharedPreferences.getString("remoteallow", "false").equals("true")) {
            cardview.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
            cardview.setVisibility(View.VISIBLE);
        }
    }

    public void updatedData() {
        mFirebaseDBData.child(EBIOTConfig.DB_EBIOTDATA).child("iotdacs").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String data = dataSnapshot.getValue(String.class);
                convertBinary(Integer.valueOf(data));
                setUI();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //Toast.makeText(context, "OnStart ...", Toast.LENGTH_SHORT).show();
        setUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Toast.makeText(context, "OnResume ...", Toast.LENGTH_SHORT).show();
        setUI();
    }

    @Override
    public void onPause() {
        super.onPause();
        //Toast.makeText(context, "OnPause ...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
