package com.blacxcode.ebiotdev.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.blacxcode.ebiotdev.R;
import com.blacxcode.ebiotdev.activity.EBIOTSettingsActivity;
import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.blacxcode.ebiotdev.sync.EBIOTDBParseData;
import com.blacxcode.ebiotdev.util.EBIOTNotificationUtils;
import com.blacxcode.ebiotdev.util.EBIOTSetColorTheme;
import com.blacxcode.ebiotdev.util.EBIOTSetLocalLanguage;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by blacXcode on 5/15/2017.
 */

public class EBIOTSettingsFragment extends Fragment {
    private static final String TAG = EBIOTSettingsActivity.class.getSimpleName();
    private static final int READ_REQUEST_CODE = 42; //41;

    protected SharedPreferences sharedPreferences;
    protected EBIOTNotificationUtils notificationUtils;

    protected FrameLayout frameLayout;
    protected EBIOTDBParseData dbParseData;
    protected DatabaseReference mFirebaseDBData;
    protected FirebaseDatabase mFirebaseInstance;

    protected CardView hostnameCardview, soundCardview, ttsCardview, soundselectCardview, languageCardview, acsCardview;
    protected EditText hostnameEdit;
    protected Button hostnameBtn, soundsetBtn, soundbrowseBtn, stopBtn, ttsBtn, languageBtn, acsBtn;
    protected TextView soundselectRightVol, soundselectLeftVol, speechrateProgress, pitchProgress, volumeProgress, soundselectUri;
    protected SeekBar leftVolume, rightVolume, masterVolume, ttsSpeechrate, ttsPitch;
    protected ToggleButton soundToggleBtn;
    protected Locale setLang;
    protected TextToSpeech tts;
    protected AudioManager audioManager;
    protected MaterialSpinner spinnerLanguage;

    protected Context context;
    protected View view;

    private String getThemes = "EBIOTColorPink";
    private String getUri = "";
    private String getRingtonesOld = "";
    private Boolean selectFile = false;
    private int  getLang = 0, maxVolume = 0, volume = 0;
    private float getPitch = 0.0f, getSpeechrate = 0.0f, maxLeftVolume = 0.0f, maxRightVolume = 0.0f, maxSpeechrate = 0.0f, maxPitch = 0.0f;

    public EBIOTSettingsFragment() {
        // Required empty public constructor
    }

    public static EBIOTSettingsFragment newInstance() {
        return new EBIOTSettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        sharedPreferences = context.getSharedPreferences(EBIOTConfig.SHARED_PREF, MODE_PRIVATE);
        getLang = sharedPreferences.getInt("EBIOTLanguage", 0);
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, parent, false);

        notificationUtils = new EBIOTNotificationUtils(context);

        frameLayout = view.findViewById(R.id.settingFragment);

        dbParseData = new EBIOTDBParseData();

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDBData = mFirebaseInstance.getReference(EBIOTConfig.DB_EBIOT);

        hostnameCardview = view.findViewById(R.id.hostnameCardview);
        hostnameEdit = view.findViewById(R.id.hostnameEdit);
        hostnameBtn = view.findViewById(R.id.hostnameBtn);

        soundselectCardview = view.findViewById(R.id.soundselectCardview);
        soundselectUri = view.findViewById(R.id.soundselectUri);
        soundsetBtn = view.findViewById(R.id.soundsetBtn);
        soundsetBtn.setEnabled(false);
        soundbrowseBtn = view.findViewById(R.id.soundbrowseBtn);
        soundselectLeftVol = view.findViewById(R.id.soundselectLeftVol);
        leftVolume = view.findViewById(R.id.leftVolume);
        soundselectRightVol = view.findViewById(R.id.soundselectRightVol);
        rightVolume = view.findViewById(R.id.rightVolume);
        volumeProgress = view.findViewById(R.id.volumeProgress);
        masterVolume = view.findViewById(R.id.masterVolume);
        stopBtn = view.findViewById(R.id.stopBtn);

        soundCardview = view.findViewById(R.id.soundCardview);
        soundToggleBtn = view.findViewById(R.id.soundToggleBtn);

        ttsCardview = view.findViewById(R.id.ttsCardview);
        speechrateProgress = view.findViewById(R.id.speechrateProgress);
        ttsSpeechrate = view.findViewById(R.id.ttsSpeechrate);
        pitchProgress = view.findViewById(R.id.pitchProgress);
        ttsPitch = view.findViewById(R.id.ttsPitch) ;
        ttsBtn = view.findViewById(R.id.ttsBtn);

        languageCardview = view.findViewById(R.id.languageCardview);
        languageBtn = view.findViewById(R.id.languageBtn);
        spinnerLanguage = view.findViewById(R.id.spinnerLanguage);

        acsCardview = view.findViewById(R.id.acsCardview);
        acsBtn = view.findViewById(R.id.acsBtn);

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        getUri = sharedPreferences.getString("EBIOTSoundUri", "");
        getRingtonesOld = getUri;

        masterVolume.setMax(150);
        ttsSpeechrate.setMax(35);
        ttsPitch.setMax(30);

        spinnerLanguage.setItems(getResources().getString(R.string.languageIndonesian),
                getResources().getString(R.string.languageEnglish), getResources().getString(R.string.languageJapanese));
        spinnerLanguage.setSelectedIndex(getLang);

        setUI(true);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        hostnameEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        hostnameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_hostname_title));

                if (hostnameEdit.getText().toString().equals("")) {
                    builder.setMessage(getResources().getString(R.string.notification_nohostname));
                } else {
                    if (!(hostnameEdit.getText().toString().contains(".firebaseio.com/"))) {
                        builder.setMessage(getResources().getString(R.string.notification_hostname_nofirebase));
                    } else if (!(hostnameEdit.getText().toString().contains("https://"))) {
                        builder.setMessage(getResources().getString(R.string.notification_hostname_nohttps));
                    } else if ((hostnameEdit.getText().toString().contains("https://") && hostnameEdit.getText().toString().contains(".firebaseio.com"))) {
                        builder.setMessage(getResources().getString(R.string.notification_hostname));
                    }
                }

                builder.setIcon(R.drawable.ic_info_outline_black_48dp);
                builder.setPositiveButton(getResources().getString(R.string.notification_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if ((hostnameEdit.getText().toString().contains("https://") && hostnameEdit.getText().toString().contains(".firebaseio.com/"))) {
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putString("EBIOTHostname", hostnameEdit.getText().toString());
                            edit.apply();

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
                            builder1.setTitle(getResources().getString(R.string.notification_hostname_reboot));
                            builder1.setMessage(getResources().getString(R.string.notification_hostname_msg_reboot));
                            builder1.setIcon(R.drawable.ic_info_outline_black_48dp);

                            builder1.setPositiveButton(getResources().getString(R.string.notification_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = context.getPackageManager(). getLaunchIntentForPackage(context.getPackageName());
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        getActivity().finishAffinity();
                                    } else {
                                        getActivity().finish();
                                    }

                                    startActivity(intent);
                                }
                            });
                            builder1.show();
                        }
                    }
                });
                builder.show();
            }
        });

        soundsetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_soundselect_title));
                builder.setMessage(getResources().getString(R.string.notification_soundselect));
                builder.setIcon(R.drawable.ic_info_outline_black_48dp);

                builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String setUri = sharedPreferences.getString("setUri", "");
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("EBIOTSoundUri", setUri);
                        edit.apply();

                        if (notificationUtils.isPlayNotificationSound()) {
                            notificationUtils.stopNotificationSound();
                            notificationUtils.playNotificationSound();
                        } else {
                            notificationUtils.playNotificationSound();
                        }

                        soundsetBtn.setEnabled(false);
                        setUI(false);
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("EBIOTSoundUri", getRingtonesOld);
                        edit.apply();

                        Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(getRingtonesOld));
                        soundselectUri.setText(ringtone.getTitle(context));

                        soundsetBtn.setEnabled(false);
                        setUI(false);
                    }
                });
                builder.show();
            }
        });

        soundbrowseBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)  {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("audio/mpeg");
                    //startActivityForResult(intent, READ_REQUEST_CODE);
                    startActivityForResult(
                            Intent.createChooser(intent, getResources().getString(R.string.soundChoose)),
                            READ_REQUEST_CODE
                    );
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("audio/mpeg");
                    //startActivityForResult(intent, READ_REQUEST_CODE);
                    startActivityForResult(
                            Intent.createChooser(intent, getResources().getString(R.string.soundChoose)),
                            READ_REQUEST_CODE
                    );
                }
            }
        });

        leftVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxLeftVolume = 1.0f;
                float leftVol = ((float) progress)/10;

                String txtleftVol = getResources().getString(R.string.soundselectLeftVol) + leftVol + "/" + maxLeftVolume;
                soundselectLeftVol.setText(txtleftVol);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("EBIOTLeftVol", leftVol);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!notificationUtils.isPlayNotificationSound()) {
                    notificationUtils.playNotificationSound();
                }
            }
        });

        rightVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxRightVolume = 1.0f;
                float rightVol = ((float) progress)/10;

                String txtrightVol = getResources().getString(R.string.soundselectRightVol) + rightVol + "/" + maxRightVolume;
                soundselectRightVol.setText(txtrightVol);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("EBIOTRightVol", rightVol);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (!notificationUtils.isPlayNotificationSound()) {
                    notificationUtils.playNotificationSound();
                }
            }
        });

        masterVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volume = progress/10;
                String txtVolume = getResources().getString(R.string.volumeTitle) + volume + "/" + maxVolume;
                volumeProgress.setText(txtVolume);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("EBIOTVolume", progress);
                edit.apply();

                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!notificationUtils.isPlayNotificationSound()) {
                    notificationUtils.playNotificationSound();
                }
            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationUtils.stopNotificationSound();
            }
        });

        soundToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    soundToggleBtn.setTextColor(Color.WHITE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        soundToggleBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
                    } else {
                        soundToggleBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
                    }

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTCheckSound", "ON");
                    edit.apply();
                } else {
                    soundToggleBtn.setTextColor(Color.GRAY);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        soundToggleBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
                    } else {
                        soundToggleBtn.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
                    }

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("EBIOTCheckSound", "OFF");
                    edit.apply();
                }
            }
        });

        ttsSpeechrate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxSpeechrate = ((float)ttsSpeechrate.getMax())/10;
                float speechrate = ((float) progress)/10;

                String txtSpeech = getResources().getString(R.string.speechrateTitle) + speechrate + "/" + maxSpeechrate;
                speechrateProgress.setText(txtSpeech);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("EBIOTSpeechrate", speechrate);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speak(getResources().getString(R.string.speakConf));
            }
        });

        ttsPitch.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxPitch = ((float)ttsPitch.getMax())/10;
                float pitch = ((float) progress)/10;

                String txtPitch = getResources().getString(R.string.pitchTitle) + pitch + "/" + maxPitch;
                pitchProgress.setText(txtPitch);

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("EBIOTPitch", pitch);
                edit.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speak(getResources().getString(R.string.speakConf));
            }
        });

        ttsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                maxSpeechrate = ((float)ttsSpeechrate.getMax())/10;
                maxPitch = ((float)ttsPitch.getMax())/10;

                float speechrate = 1.0f;
                float pitch = 1.0f;

                String txtSpeech = getResources().getString(R.string.speechrateTitle) + speechrate + "/" + maxSpeechrate;
                String txtPitch = getResources().getString(R.string.pitchTitle) + pitch + "/" + maxPitch;

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat("EBIOTSpeechrate", speechrate);
                edit.putFloat("EBIOTPitch", pitch);
                edit.apply();

                speechrateProgress.setText(txtSpeech);
                pitchProgress.setText(txtPitch);

                speechrate = speechrate*10;
                ttsSpeechrate.setProgress(Math.round(speechrate));
                ttsSpeechrate.refreshDrawableState();

                pitch = pitch*10;
                ttsPitch.setProgress(Math.round(pitch));
                ttsPitch.refreshDrawableState();

                speak(getResources().getString(R.string.speakConf));
            }
        });

        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR || status == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Snackbar.make(view, getResources().getString(R.string.ttsLangNotSupported), Snackbar.LENGTH_LONG).show();
                } else {
                    getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
                    getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);

                    float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
                    float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

                    setLang = Locale.ENGLISH;
                    tts.setLanguage(setLang);
                    tts.setPitch(pitch);
                    tts.setSpeechRate(speechrate);
                }
            }
        });

        spinnerLanguage.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>(){

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, getResources().getString(R.string.languageChoose) + item + " (" + position + ")", Snackbar.LENGTH_LONG).show();
                SharedPreferences.Editor setLang = sharedPreferences.edit();
                setLang.putInt("EBIOTLanguage", position);
                setLang.apply();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_language_title));
                builder.setMessage(getResources().getString(R.string.notification_language));
                builder.setPositiveButton(getResources().getString(R.string.notification_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());
                        Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                        //Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            getActivity().finishAffinity();
                        } else {
                            getActivity().finish();
                        }
                    }
                });
                builder.show();

            }
        });

        acsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                //AlertDialog.Builder builder = new AlertDialog.Builder(lContext, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(getResources().getString(R.string.notification_acs_title));
                builder.setMessage(getResources().getString(R.string.notification_acs));
                builder.setIcon(R.drawable.ic_info_outline_black_48dp);

                builder.setPositiveButton(getResources().getString(R.string.notification_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        reboot();
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.notification_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        });
    }

    public void setUI(Boolean animated) {
        getThemes = sharedPreferences.getString("EBIOTThemes", "EBIOTColorPink");

        frameLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor100(getThemes), null));
        hostnameCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        soundselectCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        soundCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        ttsCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        languageCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));
        acsCardview.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor300(getThemes), null));

        hostnameBtn.setTextColor(Color.WHITE);
        soundbrowseBtn.setTextColor(Color.WHITE);
        if(soundsetBtn.isEnabled()) {
            soundsetBtn.setTextColor(Color.WHITE);
        } else {
            soundsetBtn.setTextColor(Color.GRAY);
        }
        stopBtn.setTextColor(Color.WHITE);
        ttsBtn.setTextColor(Color.WHITE);
        languageBtn.setTextColor(Color.WHITE);
        acsBtn.setTextColor(Color.WHITE);

        spinnerLanguage.setTextColor(Color.WHITE);
        spinnerLanguage.setHintTextColor(Color.WHITE);
        spinnerLanguage.setLinkTextColor(Color.BLUE);
        spinnerLanguage.setBackgroundColor(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            hostnameBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            languageBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            soundbrowseBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            if (soundsetBtn.isEnabled()) {
                soundsetBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                soundsetBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorAccent(getThemes), null)));
            }
            stopBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            ttsBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            acsBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
        } else {
            hostnameBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            languageBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            soundbrowseBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            if(soundsetBtn.isEnabled()) {
                soundsetBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            } else {
                soundsetBtn.setBackgroundColor(EBIOTSetColorTheme.getColorAccent(getThemes));
            }
            stopBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            ttsBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
           acsBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
        }

        if(sharedPreferences.getString("EBIOTCheckSound", "ON").equals("ON")) {
            soundToggleBtn.setChecked(true);
            soundToggleBtn.setTextColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundToggleBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColorPrimary(getThemes), null)));
            } else {
                soundToggleBtn.setBackgroundColor(EBIOTSetColorTheme.getColorPrimary(getThemes));
            }
        } else {
            soundToggleBtn.setChecked(false);
            soundToggleBtn.setTextColor(Color.GRAY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundToggleBtn.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), EBIOTSetColorTheme.getColor900(getThemes), null)));
            } else {
                soundToggleBtn.setBackgroundColor(EBIOTSetColorTheme.getColor900(getThemes));
            }
        }

        hostnameEdit.setHint(sharedPreferences.getString("EBIOTHostname", ""));

        if (sharedPreferences.getString("remoteallow", "false").equals("true")) {
            acsCardview.setVisibility(View.VISIBLE);
            acsCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_remote_show_up));
        } else {
            acsCardview.setVisibility(View.GONE);
            acsCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_remote_show_up));
        }

        float getLeftVol = sharedPreferences.getFloat("EBIOTLeftVol", 0.0f);
        float leftVol = getLeftVol*10;
        leftVolume.setProgress(Math.round(leftVol));
        leftVolume.refreshDrawableState();
        maxLeftVolume = 1.0f;
        String getleftVolume = getResources().getString(R.string.soundselectLeftVol) + getLeftVol + "/" + maxLeftVolume;
        soundselectLeftVol.setText(getleftVolume);

        float getRightVol = sharedPreferences.getFloat("EBIOTRightVol", 0.0f);
        float rightVol = getRightVol*10;
        rightVolume.setProgress(Math.round(rightVol));
        rightVolume.refreshDrawableState();
        maxRightVolume = 1.0f;
        String getRightVolume = getResources().getString(R.string.soundselectRightVol) + getRightVol + "/" + maxRightVolume;
        soundselectRightVol.setText(getRightVolume);

        int getVol = sharedPreferences.getInt("EBIOTVolume", 0);
        masterVolume.setProgress(getVol);
        masterVolume.refreshDrawableState();
        volume = masterVolume.getProgress()/10;
        String getVolume = getResources().getString(R.string.volumeTitle) + volume + "/" + maxVolume;
        volumeProgress.setText(getVolume);

        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        float speechrate = getSpeechrate*10;
        ttsSpeechrate.setProgress(Math.round(speechrate));
        ttsSpeechrate.refreshDrawableState();
        maxSpeechrate = ((float)ttsSpeechrate.getMax())/10;
        String txtSpeech = getResources().getString(R.string.speechrateTitle) + getSpeechrate + "/" + maxSpeechrate;
        speechrateProgress.setText(txtSpeech);

        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);
        float pitch = getPitch*10;
        ttsPitch.setProgress(Math.round(pitch));
        ttsPitch.refreshDrawableState();
        maxPitch = ((float)ttsPitch.getMax())/10;
        String txtPitch = getResources().getString(R.string.pitchTitle) + getPitch + "/" + maxPitch;
        pitchProgress.setText(txtPitch);

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("EBIOTVolume", masterVolume.getProgress());
        edit.putFloat("EBIOTSpeechrate", getSpeechrate);
        edit.putFloat("EBIOTPitch", getPitch);
        edit.apply();

        if (animated) {
            hostnameCardview.setVisibility(View.VISIBLE);
            hostnameCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_hostname_show_up));
            languageCardview.setVisibility(View.VISIBLE);
            languageCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_language_show_up));
            soundselectCardview.setVisibility(View.VISIBLE);
            soundselectCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_soundselect_show_up));
            soundCardview.setVisibility(View.VISIBLE);
            soundCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_sound_show_up));
            ttsCardview.setVisibility(View.VISIBLE);
            ttsCardview.startAnimation(AnimationUtils.loadAnimation(context, R.anim.setting_tts_show_up));
        }
    }

    @SuppressLint("Deprecation")
    private void speak(String stts) {
        getSpeechrate = sharedPreferences.getFloat("EBIOTSpeechrate", 1.0f);
        getPitch = sharedPreferences.getFloat("EBIOTPitch", 1.0f);

        float pitch = (getPitch == 0.0f) ? 0.1f : getPitch;
        float speechrate = (getSpeechrate == 0.0f) ? 0.1f : getSpeechrate;

        tts.setLanguage(setLang);
        tts.setPitch(pitch);
        tts.setSpeechRate(speechrate);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(stts, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                Uri uri = resultData.getData();
                Log.i(TAG, "setUri: " + uri.toString());

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("setUri", uri.toString());
                edit.apply();

                Snackbar.make(view, getResources().getString(R.string.snackbar_soundselect), 5000).show();
            }
        } else {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("setUri", "");
            edit.apply();
        }
    }

    public void reboot(){
        dbParseData.setIOTRST(mFirebaseDBData, "ON");
    }

    @Override
    public void onStart() {
        super.onStart();
        //Toast.makeText(context, "OnStart ...", Toast.LENGTH_SHORT).show();
        String setUri = sharedPreferences.getString("setUri", "");

        if(!setUri.equals("") && !setUri.equals(getUri)) {
            Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(setUri));
            soundselectUri.setText(ringtone.getTitle(context));
            soundsetBtn.setEnabled(true);
        } else {
            Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(getUri));
            soundselectUri.setText(ringtone.getTitle(context));
            soundsetBtn.setEnabled(false);
        }

        setUI(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Toast.makeText(context, "OnResume ...", Toast.LENGTH_SHORT).show();
        setUI(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Toast.makeText(context, "OnPause ...", Toast.LENGTH_SHORT).show();
        if (notificationUtils.isPlayNotificationSound()) {
            notificationUtils.stopNotificationSound();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //Toast.makeText(context, "onStop ...", Toast.LENGTH_SHORT).show();
        if (notificationUtils.isPlayNotificationSound()) {
            notificationUtils.stopNotificationSound();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());
        //Toast.makeText(context, "onDestroyView ...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EBIOTSetLocalLanguage.setLocale(EBIOTSetLocalLanguage.getLocale(getLang), getApplicationContext());
        //Toast.makeText(context, "onDestroy ...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //Toast.makeText(context, "onDetach ...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
