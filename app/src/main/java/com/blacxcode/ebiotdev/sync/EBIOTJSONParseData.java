package com.blacxcode.ebiotdev.sync;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.facebook.FacebookSdk.getCacheDir;

/**
 * Created by blacXcode on 5/8/2017.
 */

public class EBIOTJSONParseData {
    public EBIOTJSONParseData() {
        // Required empty public constructor
    }

    public static String loadJSONFromAsset(Context context, String filename) {
        String json;
        int no_bytes_read;

        try {
            InputStream inputStream = context.getAssets().open(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            no_bytes_read = inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

//    private void writeFile() {
//
//        File extStore = Environment.getExternalStorageDirectory();
//        // ==> /storage/emulated/0/note.txt
//        String path = extStore.getAbsolutePath() + "/" + fileName;
//        Log.i("ExternalStorageDemo", "Save to: " + path);
//
//        try {
//            File myFile = new File(path);
//            myFile.createNewFile();
//            FileOutputStream fOut = new FileOutputStream(myFile);
//            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
//            myOutWriter.append(data);
//            myOutWriter.close();
//            fOut.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }



//    private void readFile() {
//
//        File extStore = Environment.getExternalStorageDirectory();
//        // ==> /storage/emulated/0/note.txt
//        String path = extStore.getAbsolutePath() + "/" + fileName;
//        Log.i("ExternalStorageDemo", "Read file: " + path);
//
//        String s = "";
//        String fileContent = "";
//        try {
//            File myFile = new File(path);
//            FileInputStream fIn = new FileInputStream(myFile);
//            BufferedReader myReader = new BufferedReader(
//                    new InputStreamReader(fIn));
//
//            while ((s = myReader.readLine()) != null) {
//                fileContent += s + "\n";
//            }
//            myReader.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Toast.makeText(getApplicationContext(), fileContent, Toast.LENGTH_LONG).show();
//    }

    // Create a file in the Internal Storage
    public static void writeToInternalFile(Context context, String fileName, String content) {
        FileOutputStream outputStream;

        try {
            File file = new File(context.getFilesDir(), fileName);
            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readFromInternalFile(Context context, String fileName) {
        String data = "";
        FileInputStream inputStream;
        try {
            inputStream = context.openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            data = stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    // Create a file in the Cache
    public static void writeToInternalCache(String fileName, String content) {
        File file;
        FileOutputStream outputStream;
        try {
            // file = File.createTempFile("MyCache", null, getCacheDir());
            file = new File(getCacheDir(), fileName);

            outputStream = new FileOutputStream(file);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFromInternalCache(String fileName) {
        String data = "";
        try {
            String line;
            File file = new File(getCacheDir(), fileName); // Pass getFilesDir() and "MyFile" to read file
            BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            StringBuilder buffer = new StringBuilder();
            while ((line = input.readLine()) != null) {
                buffer.append(line);
            }

            data = buffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static void writeToExternalFile(String dirName, String fileName, String content) {
        //boolean mExternalStorageAvailable = false;
        //boolean mExternalStorageWriteable = false;

        FileOutputStream outputStream;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            //mExternalStorageAvailable = mExternalStorageWriteable = true;
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath() + dirName);

            boolean isDirectoryCreated = dir.exists();
            if (!isDirectoryCreated) {
                dir.mkdirs();
                Log.e("writeToExternalFile", "create directory");
            } else {
                File file = new File(dir, fileName);

                try {
                    outputStream = new FileOutputStream(file);
                    outputStream.write(content.getBytes());
                    outputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("writeToExternalFile", "******* File not found. Did you" +
                            " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            //mExternalStorageAvailable = true;
            //mExternalStorageWriteable = false;
            Log.e("writeToExternalFile", "******* Can only read the media" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } else {
            // Can't read or write
            //mExternalStorageAvailable = mExternalStorageWriteable = false;
            Log.e("writeToExternalFile", "******* Can't read or write" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        }
    }

    public static String readFromExternalFile(String dirName, String fileName) {
        String data = "";
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                String line;

                String filepath = Environment.getExternalStorageDirectory().toString();
                File dir = new File(filepath, dirName);
                File file = new File(dir, fileName);

                BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    buffer.append(line);
                }

                data = buffer.toString();
                Log.e("readFromExternalFile", "buffer.toString()");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static Target imageDownloading(Context context, final String imageDir, final String imageName) {
        Log.d("picassoImageTarget", " picassoImageTarget");
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE); // path to /data/data/yourapp/app_imageDir
        return new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {

                }
            }
        };
    }
}
