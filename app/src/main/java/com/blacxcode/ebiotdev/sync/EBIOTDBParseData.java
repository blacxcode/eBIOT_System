package com.blacxcode.ebiotdev.sync;

import com.blacxcode.ebiotdev.config.EBIOTConfig;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by blacXcode on 5/10/2017.
 */

public class EBIOTDBParseData {
    private static final String TAG = EBIOTDBParseData.class.getSimpleName();

    public String id;
    public String deviceid;
    public String devicekey;
    public String remoteallow;
    public String username;
    public String iotrst;
    public String iotacs;
    public String iotdacs;
    public String iotbtn;
    public String iotmsg;
    public String iotsnd;

    public EBIOTDBParseData() {
        // Required empty public constructor
    }

    public void updateClient(DatabaseReference mFirebaseDatabase, String id, String deviceid, String devicekey, String remoteallow, String username) {
        this.id = id;
        this.deviceid = deviceid;
        this.devicekey = devicekey;
        this.remoteallow = remoteallow;
        this.username = username;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTCLIENT).child(id).child("deviceid").setValue(this.deviceid);
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTCLIENT).child(id).child("devicekey").setValue(this.devicekey);
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTCLIENT).child(id).child("remoteallow").setValue(this.remoteallow);
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTCLIENT).child(id).child("username").setValue(this.username);
    }

    public void setIOTRST(DatabaseReference mFirebaseDatabase, String iotrst) {
        this.iotrst = iotrst;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotrst").setValue(this.iotrst);
    }

    public void setIOTACS(DatabaseReference mFirebaseDatabase, String iotacs) {
        this.iotacs = iotacs;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotacs").setValue(this.iotacs);
    }

    public void setIOTDACS(DatabaseReference mFirebaseDatabase, String iotdacs) {
        this.iotdacs = iotdacs;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotdacs").setValue(this.iotdacs);
    }

    public void setIOTBTN(DatabaseReference mFirebaseDatabase, String iotbtn) {
        this.iotbtn = iotbtn;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotbtn").setValue(this.iotbtn);
    }

    public void setIOTMSG(DatabaseReference mFirebaseDatabase, String iotmsg) {
        this.iotmsg = iotmsg;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotmsg").setValue(this.iotmsg);
    }

    public void setIOTSND(DatabaseReference mFirebaseDatabase, String iotsnd) {
        this.iotsnd = iotsnd;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child("iotsnd").setValue(this.iotsnd);
    }

    public void removeClient(DatabaseReference mFirebaseDatabase, String id) {
        this.id = id;

        // updating the user via child nodes
        mFirebaseDatabase.child(EBIOTConfig.DB_EBIOTDATA).child(this.id).removeValue();
    }
}
